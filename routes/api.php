<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\DashboardController;
  //pass : 7rDTfLwcaGR3AZ6e
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(DashboardController::class)->prefix('dashboard')->group(function () {

    
    //auth
    Route::get('/index', 'index');
    Route::post('/upload', 'upload');
    Route::post('/bulkUpdate', 'bulkUpdate');
    Route::post('/user/login', 'AccountLogin');

    Route::post('/client/auth/add', 'addClientAuth');
    Route::get('/client/auth/email/{email}', 'getClientAuthByEmail');


    Route::post('/client/login', 'clientLogin');
    Route::get('/user/session/{session_id}', 'getUserSession');

    Route::post('/user/contact/add', 'addContact');
    Route::post('/user/note/add', 'addNote');
    Route::patch('/user/note/{note_id}/edit', 'updateNote');
    Route::delete('/user/note/{note_id}/delete', 'deleteNote');

    Route::delete('/user/contact/{contact_id}/delete', 'deleteContact');
    Route::get('/user/contact/', 'getContact');
    Route::patch('/user/contact/{contact_id}/edit', 'updateContact');


    //user
    Route::get('/user/account', 'getUserAccount');
    Route::get('/user/role', 'getUserRole');
    Route::get('/user/group/{group_id}', 'getUserGroupById');
    Route::get('/user/group', 'getUserGroup');
    Route::get('/user/menu_access', 'getUserMenuAccess');

    Route::post('/user/account/add', 'addUserAccount');
    Route::post('/user/task/add', 'addUserAccountTask');


    Route::patch('/user/task/{task_id}/edit', 'updateUserTask');

    Route::post('/user/task_discussion/add', 'addUserAccountTaskDiscussion');
    Route::post('/user/menu_access/add', 'addMenuAccess');


    Route::patch('/user/account/{account_id}/edit', 'updateUserAccount');

   

        Route::get('/user/account/{account_id}/notification/{notification_type}', 'getUserAccountNotification');
        Route::get('/user/account/{account_id}/note/', 'getUserAccountNote');
        Route::get('/user/account/{account_id}/task', 'getUserAccountTask');
        Route::get('/user/account/{account_id}/task_discussion/{task_id}', 'getUserAccountTaskDiscussion');
        


    //creator
    Route::get('/creator/creator', 'getCreator');
    Route::get('/creator/dashboard/{type}', 'getCreatorDashboard');
    Route::get('/creator/management', 'getManagement');
    Route::get('/creator/management/{id}', 'getManagementById');

    Route::post('/creator/management/add', 'addManagement');
    Route::patch('/creator/management/{id}/edit', 'updateManagement');
    Route::delete('/creator/management/{id}/delete', 'deleteManagement');



    Route::post('/creator/creator/add', 'addCreator');
    Route::patch('/creator/creator/{creator_id}/edit', 'updateCreator');

    Route::post('/creator/performance/add', 'addPerformance');
    Route::patch('/creator/performance/{performance_id}/edit', 'updatePerformance');

        //creator-view
        Route::get('/creator/creator/{creator_id}', 'getCreatorById');
        Route::get('/creator/creator/{creator_id}/contact', 'getCreatorPersonContact');
        Route::get('/creator/creator/{creator_id}/note', 'getCreatorPersonNote');
        Route::get('/creator/creator/{creator_id}/contract', 'getCreatorPersonContract');
        Route::get('/creator/creator/{creator_id}/social_metric', 'getCreatorPersonSocialMetric');
        Route::get('/creator/creator/{creator_id}/rate_cart', 'getCreatorPersonRateCart');

        Route::post('/creator/creator/{creator_id}/rate_cart/add', 'addCreatorRate');
        Route::patch('/creator/rate_cart/{rate_id}/edit', 'updateCreatorRate');
        
        Route::delete('/creator/rate_cart/{rate_id}/delete', 'deleteCreatorRate');


        


        //campaign-view
        Route::get('/creator/campaign/', 'getCampaign');
        Route::get('/creator/campaign/{campaign_id}', 'getCampaignById');
        Route::post('/creator/campaign/add', 'addCampaign');
        Route::patch('/creator/campaign/{campaign_id}/edit', 'updateCampaign');

         Route::post('/creator/creator_campaign/add', 'addInvitedCreator');
         Route::delete('/creator/creator_campaign/{campaign_id}/delete', 'deleteInvitedCreator');
    

         Route::post('/creator/creator_campaign/{creator_campaign_id}/contract/add', 'addContract');
         Route::get('/creator/contract/', 'getContract');
         Route::patch('/creator/contract/{contract_id}/edit', 'updateContract');
         
    
    //client
   // Route::post('/newsfeed/account/add', 'addContact');

   Route::post('/client/newsfeed/account/add', 'addNewsfeedAccount');
   Route::post('/client/newsfeed/account/login', 'newsfeedAccountLogin');
   Route::get('/client/newsfeed/account/session/{session_id}', 'getAuthorSession');
   
   Route::get('/client/newsfeed/account/{account_id}', 'getNewsfeedAccountById');


    Route::get('/client/newsfeed/status/{status}/post/', 'getPost');
    Route::get('/client/newsfeed/recommended_post/', 'getRecommendedPost');

    Route::post('/client/newsfeed/post/add', 'addPost');
    Route::patch('/client/newsfeed/post/{post_id}/edit', 'updatePost');

    Route::post('/client/newsfeed/reaction/add', 'addReaction');
  

    Route::get('/client/newsfeed/post/{post_id}', 'getPostById');
    Route::get('/client/newsfeed/category/type/{type}', 'getPostCategory');
    Route::post('/client/newsfeed/category/add', 'addPostCategory');
   
    Route::post('/client/newsfeed/term/add', 'addPostTerm');
   
    Route::post('/client/newsfeed/discussion/add', 'addPostDiscussion');

   
    Route::delete('/client/newsfeed/term/{term_id}/delete', 'removePostTerm');

    Route::patch('/client/newsfeed/category/{cat_id}/edit', 'updatePostCategory');

    Route::delete('/client/newsfeed/category/{cat_id}/delete', 'deletePostCategory');
    

    Route::get('/client/newsfeed/tag', 'getPostTag');
   
    Route::get('/client/newsfeed/keyword_filter', 'getPostKeywordFilter');
    Route::post('/client/newsfeed/keyword_filter/add', 'addPostKeywordFilter');

    Route::patch('/client/newsfeed/keyword_filter/{id}/edit', 'updatePostKeywordFilter');
    Route::delete('/client/newsfeed/keyword_filter/{id}/delete', 'deleteKeywordFilter');



    Route::get('/client/newsfeed/account', 'getNewsfeedAccount');
    Route::get('/client/newsfeed/contact', 'getNewsfeedContact');
    Route::post('/client/newsfeed/contact/add', 'addNewsfeedContact');

    
    Route::get('/client/client/type/{type}', 'getClient');
    Route::get('/client/clien/{client_id}', 'getClientById');
    
    Route::post('/client/client/add', 'addClient');

    Route::patch('/client/client/{client_id}/edit', 'updateClient');


    Route::delete('/client/client/{client_id}/delete', 'deletClient');

    

    Route::get('/client/aquisition', 'getAquisition');
    Route::get('/client/aquisition/{aquisition_id}', 'getAquisitionById');
    Route::patch('/client/aquisition/{aquisition_id}/edit', 'updateAquisition');
    Route::post('/client/aquisition/add', 'addAquisition');
    
    Route::get('/client/scope', 'getScope');
    Route::post('/client/scope/add', 'addScope');   
    Route::patch('/client/scope/{scope_id}/edit', 'updateScope');   


    //channel
    Route::get('/channel/vendor/type/{type}', 'getVendor');
    Route::get('/channel/vendor/{vendor_id}', 'getVendorById');
    Route::delete('/channel/vendor/{id}/delete', 'deleteVendor');
    Route::post('/channel/vendor/add', 'addVendor');

    Route::patch('/channel/vendor/{vendor_id}/edit', 'updateVendor');

    Route::get('/channel/product/type/{type}', 'getVendorProduct');
    Route::post('/channel/product/add', 'addVendorProduct');
    Route::delete('/channel/product/{product_id}/delete', 'deleteVendorProduct');


    Route::patch('/channel/product/{product_id}/edit', 'updateVendorProduct');

    Route::get('/channel/inventory/type/{type}', 'getInventory');
    Route::post('/channel/inventory/add', 'addInventory');
    Route::delete('/channel/inventory/type/{type}/id/{id}/delete', 'deleteInventory');

    Route::patch('/channel/inventory/{inventory_id}/edit', 'updateInventory');

    Route::post('/channel/inventory/{inventory_id}/stock/add', 'addInventoryStock');
    Route::patch('/channel/inventory/{inventory_id}/stock/{stock_id}/edit', 'updateInventoryStock');
   
    Route::get('/channel/product/category/type/{type}', 'getVendorProductCategory');
    Route::post('/channel/product/category/add', 'addVendorProductCategory');
    Route::patch('/channel/product/category/{category_id}/edit', 'updateVendorProductCategory');
    Route::delete('/channel/product/category/{category_id}/delete', 'deleteVendorProductCategory');
    

    Route::get('/channel/employee', 'getEmployee');
    Route::get('/channel/employee/{employee_id}', 'getEmployeeById');
    Route::post('/channel/employee/add', 'addEmployee');
    Route::patch('/channel/employee/{employee_id}/edit', 'updateEmployee');


    Route::post('/channel/employee/education/add', 'addEmployeeEducation');
    Route::delete('/channel/employee/education/{education_id}/delete', 'deleteEmployeeEducation');
   

    Route::post('/channel/employee/experience/add', 'addEmployeeExperience');
    Route::delete('/channel/employee/experience/{experience_id}/delete', 'deleteEmployeeExperience');
   
   
    Route::post('/channel/employee/performance/add', 'addEmployeePerformance');
    Route::patch('/channel/employee/performance/{performance_id}/edit', 'updateEmployeePerformance');

    //community

    Route::get('/community/community', 'getCommunity');

    Route::get('/community/member', 'getCommunityMember');
    Route::get('/community/member/{member_id}', 'getCommunityMemberById');




    Route::get('/community/campaign', 'getCommunityCampaign');
    Route::get('/community/campaign/{campaign_id}', 'getCommunityCampaignById');
    Route::post('/community/campaign/add', 'addCommunityCampaign');
    Route::patch('/community/campaign/{campaign_id}/edit', 'updateCommunityCampaign');
    
    Route::post('/community/campaign/{campaign_id}/member/add', 'addCommunityCampaignMember');
    Route::patch('/community/campaign-member/{member_id}/edit', 'updateCommunityCampaignMember');
    

    Route::post('/community/member/add', 'addCommunityMember');
    Route::patch('/community/member/{member_id}/edit', 'updateCommunityMember');
    
    Route::get('/community/points/', 'getCommunityPoints');
    Route::get('/community/points/leaderboard', 'getPointsLeaderboard');
    Route::get('/community/points/collection', 'getCommunityPointsCollection');
    Route::post('/community/points/collection/add', 'addCommunityPointsCollection');
    Route::patch('/community/points/collection/{collection_id}/edit', 'updateCommunityPointsCollection');
    Route::post('/community/points/add', 'addMemberPoints');
    Route::post('/community/points/{id}/edit', 'updateMemberPoints');
    Route::post('/community/member/{member_id}/points/add', 'addCampaignPoints');


    Route::get('/community/reward/', 'getReward');
    Route::get('/community/reward/redeem/', 'getRewardRedeem');
    Route::patch('/community/reward/redeem/{redeem_id}/edit', 'updateRewardRedeem');

    Route::post('/community/reward/{reward_id}/member/{member_id}/redeem/add', 'addMemberRedeem');
    Route::post('/community/reward/add', 'addReward');
    Route::post('/community/member/{member_id}/reward/add', 'addMemberReward');
    Route::post('/community/custom_reward/add', 'addCustomMemberReward');
   

    Route::get('/community/ticket/', 'getTicket');
    Route::get('/community/ticket/{ticket_id}', 'getTicketById');
    Route::get('/community/member/{member_id}/ticket', 'getTicketByMemberId');

    Route::post('/community/ticket/discussion/add', 'addTicketDiscussion');
    Route::patch('/community/ticket/{ticket_id}/edit', 'updateTicket');
    Route::post('/community/ticket/add', 'addTicket');
   
    
});


