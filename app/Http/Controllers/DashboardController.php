<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use App\Models\Account;
use App\Models\Group;
use App\Models\Role;
use App\Models\MenuAccess;
use App\Models\Notification;
use App\Models\Note;
use App\Models\Task;
use App\Models\TaskDiscussion;
use App\Models\TaskMember;

use App\Models\Creator;

use App\Models\CreatorManagement;
use App\Models\RateCart;
use App\Models\SocialMetric;
use App\Models\Persona;
use App\Models\Performance;


use App\Models\Client;
use App\Models\Campaign;
use App\Models\ClientAquisition;
use App\Models\ScopeWork;
use App\Models\CreatorCampaign;
use App\Models\Contract;
use App\Models\Contacts;
use App\Models\Vendor;
use App\Models\VendorProduct;
use App\Models\VendorProductCategory;

use App\Models\Inventory;
use App\Models\InventoryStock;

use App\Models\Employee;
use App\Models\EmployeeEducation;
use App\Models\EmployeeExperience;
use App\Models\EmployeePerformance;

use App\Models\Community;
use App\Models\CommunityMember;
use App\Models\CommunityCampaign;
use App\Models\CommunityCampaignMember;

use App\Models\CommunityPoints;


use App\Models\CommunityPointsCollection;
use App\Models\CommunityReward;
use App\Models\CommunityRewardRedeem;

use App\Models\CommunityTicket;
use App\Models\CommunityTicketDiscussion;

use App\Models\ClientAuth;

use App\Models\NewsfeedPost;
use App\Models\NewsfeedCategory;
use App\Models\NewsfeedPostCategory;
use App\Models\NewsfeedDiscussion;

use App\Models\NewsfeedReaction;

use App\Models\NewsfeedAccount;
use App\Models\NewsfeedKeywordFilter;
use App\Models\NewsfeedContactForm;




use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;


class DashboardController extends Controller
{
    //GLOBAL FUNCTION START
    //pass : 7rDTfLwcaGR3AZ6e

    private function _isValidated($token){
        if($token == 'token'){
            return true;
        }else{
            return true;
        }
       
    }

    private function _hasAccess($account){
        //validate n return
        return true;
    }


    private function _getUserAccountById($account_id){
        $account = Account::find($account_id);
        return $account;
    }
    private function _getUserAccountByEmail($email){
        $account = Account::find($account_id);
        return $account;
    }

    private function _getUserMenuAccessById($account_id){
        //return obj
        return true;
    }

    private function _addUserAccountNotification($account_id,$title,$desc,$nav){
        $post = array(
            'title'=>$title,
            'description'=>$desc,
            'account_id'=>$account_id,
            'notification_type'=>'notification',
           

         );

         $data = Notification::create($post);
    }

    private function _addAccountActivity($account_id,$title,$desc,$nav ){
       
        $post = array(
            'title'=>$title,
            'description'=>$desc,
            'account_id'=>$account_id,
            'notification_type'=>'activity',
           

         );

         $data = Notification::create($post);
            // return true;
    }
   
   
    //GLOBAL FUNCTION END
    public function upload(Request $request)
    {   
        

        if($this->_isValidated($request->bearerToken())){
            try {

                
                $file = $request->file('file');
                $nama_file = time()."_".$file->getClientOriginalName();
        
                $tujuan_upload = 'data_file';
                $file->move($tujuan_upload,$nama_file);

                $path = 'http://103.41.205.129/data_file/'.$nama_file;
                
           

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $path,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);



       
    }

    public function _uploadFile($file)
    {   
        
        $nama_file = time()."_".$file->getClientOriginalName();
        $tujuan_upload = 'data_file';
        $file->move($tujuan_upload,$nama_file);

        return $nama_file;


      



       
    }

    public function bulkUpdate(Request $request)
    {
        // http://103.41.205.129/api/dashboard/bulkUpdate
        // $post = $request->all();
        // $data = CommunityMember::insert($post);

        // $response = array(
        //     'http_request' => 'POST',
        //     'status_code' => 200,
        //     'message_code' => 'success',
         
 
           
          
        // );

        // return Response::json($response);

    }

    public function index()
    {

       
        try {
             $data = Account::find(6);
          //   $data->delete();

        //     $data = NewsfeedPost::With('author')
        //     ->With('reaction')
    
        //     ->With('category.category')
        //     ->With('account')
        //     ->limit(8);
         
        // $result =  $data->get();


        // $sorted =  $result->sortByDesc('discussion_count');


       
       
            // $raw_data = Creator::get();
            // $raw_data = $raw_data->pluck('persona')->values()->all();

            // foreach($raw_data as $item){
                
            //     foreach (json_decode($item) as $child_item){
            //         $collection[] = $child_item;
            //     }
            // }
            // $collection=  collect($collection);
            // $grouped = $collection->countBy('name');
            
 
            // $grouped->all();
            // DB::table('animals')
            // ->select('name')
            // ->groupBy('name')
            // ->orderByRaw('COUNT(*) DESC')
            // ->limit(1)
            // ->get(); 
                 

         
        

 

     
            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => 'success',
                 'data' => $data,

     
               
              
            );
          
          } catch (\Exception $e) {
            $response = array(
                'status_code' => 400,
                'message_code' =>  $message_code =  $e->getMessage(),
            );
          }
       return Response::json($response);
    } 
  
    //USER START
    


    public function AccountLogin(Request $request)
    {
    
        if($this->_isValidated($request->bearerToken())){
            try {
               
                $email = $request->input('email');
                $password = $request->input('password_hash');
                $session_id = $request->input('session_id');

                $data = Account::With('role')->With('group')
                ->where('email', $email)
                ->where('password_hash',$password)
                ->first();
                
                if($data->status == true){
                    $data->session_id=$session_id;
                    $data->save();

                    $response = array(
                        'http_request' => 'POST',
                        'status_code' => 200,
                        'message_code' => 'success',
                        'endpoint' => $request->url(),
                        'data' =>  $data,
                       
                    );
                }else{
                 
                    $response = array(
                        
                        'status_code' => 400,
                        'message_code' => 'email or password incorrect, please contact your team leader',
                     
                    );
                }

              
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);


      
       
        
       
    } 

    public function newsfeedAccountLogin(Request $request)
    {
    
        if($this->_isValidated($request->bearerToken())){
            try {
               
                $email = $request->input('email');
                $password = $request->input('password_hash');
                $session_id = $request->input('session_id');

                $data = NewsfeedAccount::where('email', $email)
                ->where('password_hash',$password)
                ->first();
                
                if($data){
                    $data->session_id=$session_id;
                    $data->save();

                    $response = array(
                        'http_request' => 'POST',
                        'status_code' => 200,
                        'message_code' => 'success',
                        'endpoint' => $request->url(),
                        'data' =>  $data,
                       
                    );
                }else{
                 
                    $response = array(
                        
                        'status_code' => 400,
                        'message_code' => 'email or password incorrect, please contact your team leader',
                     
                    );
                }

              
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);


      
       
        
       
    } 


    
    public function addClientAuth(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = ClientAuth::create($post);
           

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);


        

      
    } 

    public function getClientAuthByEmail($email)
    {
    
        try {
               

            $data = ClientAuth::where('username', $email)
            ->first();
                 

            
            if($data){
                
              

               
                $response = array(
                    'http_request' => 'GET',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'data' => $data,
                   
                );
            }else{
                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'Not Exist',
                    'data' => null,
                   
                );
            }
            
          
          } catch (\Exception $e) {
            $response = array(
                'status_code' => 400,
                'message_code' =>  $message_code =  $e->getMessage(),
            );
          }

        return Response::json($response);


      
       
        
       
    } 

    

    

 
    
    public function clientLogin(Request $request)
    {
    
        if($this->_isValidated($request->bearerToken())){
            try {
               
                $email = $request->input('username');
                $password = $request->input('password');
                
               
                

                $data = ClientAuth::where('username', $email)
                ->where('password',$password)
                
                ->first();
                
                if($data){
                    
                    $response = array(
                        'http_request' => 'POST',
                        'status_code' => 200,
                        'message_code' => 'success',
                        'endpoint' => $request->url(),
                        'data' =>  $data,
                       
                    );
                }else{
                 
                    $response = array(
                        
                        'status_code' => 400,
                        'message_code' => 'email or password incorrect, please contact admin',
                     
                    );
                }

              
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);


      
       
        
       
    } 


    public function getUserSession($session_id)
    {
    
        try {
               
               
            $data = Account::
            where('session_id',$session_id)->With('role')->With('group')->
            get();

            
            if($data->count() > 0){
                
              

               
                $response = array(
                    'http_request' => 'GET',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'data' =>  $data[0],
                   
                );
            }else{
                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 400,
                    'message_code' => 'Email or password not correct, please contact your team leader',
                   
                );
            }
            
          
          } catch (\Exception $e) {
            $response = array(
                'status_code' => 400,
                'message_code' =>  $message_code =  $e->getMessage(),
            );
          }

        return Response::json($response);


      
       
        
       
    } 

    
    public function getAuthorSession($session_id)
    {
    
        try {
               
               
            $data = NewsfeedAccount::where('session_id',$session_id)->first();

            
            if($data->id){
                
              

               
                $response = array(
                    'http_request' => 'GET',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'data' =>  $data,
                   
                );
            }else{
                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 400,
                    'message_code' => 'Email or password not correct, please contact your team leader',
                   
                );
            }
            
          
          } catch (\Exception $e) {
            $response = array(
                'status_code' => 400,
                'message_code' =>  $message_code =  $e->getMessage(),
            );
          }

        return Response::json($response);


      
       
        
       
    } 



    public function getUserAccount()
    {
    

        $data = Account::With('role')->With('group')->With('group')->get();
        
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => 'dasdas',
            'endpoint' => 'user_account',
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function getContact()
    {
    

        $data = Contacts::With('creator')->With('client')->get();
        
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => 'dasdas',
            'endpoint' => 'user_account',
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function addContact(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = Contacts::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);


        

      
    } 

    
    public function addNote(Request $request)
    {
        
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
         
            
            // $post = array(
            //     'description' => 'credit',
            // 'campaign_id'=> '1'
            // );
            $data = Note::create($post);

      

              //  $result=$data->With('account');
          
            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => 'success',
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function updateNote($note_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = Note::find($note_id)->update($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);

        

      
    } 

    public function deleteNote($note_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = Note::find($note_id)->delete();

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);

        

      
    } 
    
    

    public function updateContact($contact_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = Contacts::find($contact_id)->update($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);

        

      
    } 
    

    public function addUserAccount(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = Account::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);


        

      
    } 

    public function addNewsfeedAccount(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $email = $request->input('email');
                $password = $request->input('password_hash');
                $first_name = $request->input('first_name');
                $session_id = $request->input('session_id');


                $post = array(
                    'email'=> $email,
                    'first_name' => $first_name,
                    'password_hash' => $password,
                    'session_id' => $session_id
    
                );
    
                
                $data = NewsfeedAccount::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);


        

      
    } 

    public function updateUserAccount($account_id,Request $request)
    {
        
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = Account::find($account_id)->update($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 



   

    public function getUserRole()
    {
      
        $data = Role::all();
        
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => 'dasdas',
            'endpoint' => 'user_role',
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function getUserGroupById($role_id)
    {
      
        $data = Group::With('accounts')->find($role_id);
        
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => 'dasdas',
            'endpoint' => 'user_role',
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function getUserGroup()
    {
        $data = Group::With('accounts')->get();
        
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => 'dasdas',
            'endpoint' => 'user_group',
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function getUserMenuAccess(Request $request)
    {
        $data = MenuAccess::all();
        
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => 'dasdas',
            'endpoint' => $request->url(),
            'data' =>  $data,
            'request' =>  $request->all(),
        );
       return Response::json($response);
    } 

    public function addUserMenuAccess(Request $request)
    {
        
        if($this->_isValidated($request->bearerToken())){
            $data =  $request->email;
            $message_code = 'success';
        }
        else{
            $data =  null;
            $message_code = 'invalid token';
        }
        
   
        $response = array(
            'http_request' => 'POST',
            'status_code' => 200,
            'message_code' => $message_code,
            'endpoint' => $request->url(),
            'data' =>  $data,
            'request' =>  $request->all(),
        );
  
      
      return Response::json($response);
    } 


    public function getUserAccountNotification($account_id,$notification_type)
    {
        $data = Notification::
            where('account_id',$account_id)->
            where('notification_type',$notification_type)->
            get();

       
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => 'dasdas',
            'endpoint' => 'user_notification',
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 



   
    public function getUserAccountNote($account_id)
    {
        $data = Note::All();
        
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => 'dasdas',
            'endpoint' => 'user_task',
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 


    public function getUserAccountTask($account_id,Request $request)
    {
        $type = $request->input('tab_view');
        $status = $request->input('status');
        $priority = $request->input('priority');
        $search = $request->input('search');
        $keywords = explode(' ', $search);

        

        if($type == 'archive'){
            $data = Task::With('owner');
            $data->With('member.account');
            $data->orderBy('created_at', 'desc');

            $data->where('status','archive');

            $data->where(
                function($query) use($account_id) {
                  return $query
                           // ->where('status','archive')
                            ->where('owner_id',$account_id)
    
                            ->orWhereHas('member', function ($query) use($account_id){
                                $query->where('account_id', $account_id);
                            });
                  
                 });


        }

        else if($type == 'to_do'){
            $data = Task::With('owner');
            $data->With('member.account');
            $data->orderBy('created_at', 'desc');
            //$data->whereNot('status','archive');
          
          if($priority){
            $data->where('priority',$priority);

          }

          if($search){
            foreach($keywords as $word){

                $data->where(
                    function($query) use($word) {
                      return $query
                                ->WhereLike('title',$word)
                                ->OrWhereLike('last_name',$word);
                      
                     });
                
              
             }
           
            }

          $data->where(
            function($query) use($account_id) {
              return $query
                       //->whereNot('status','archive')
                        ->where('owner_id',$account_id)

                        ->orWhereHas('member', function ($query) use($account_id){
                            $query->where('account_id', $account_id);
                        });
              
            });

            
        }

        


        


        $result =  $data->get();

        if($type == 'archive'){
            $result =  $result
            ->where('status','archive')
        
            ->values()->all();
      
        }else if($type == 'to_do'){

           
            if($status){
                $result =  $result->where('status',$status)->values()->all();
            }else{
                $result =  $result->where('status','!=','archive')->values()->all();
            }
        
            
        }

       
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => 'dasdas',
            'endpoint' => 'user_task',
            'data' =>  $result,
          
            'request' =>  '',
        );
       return Response::json($response);
    } 



    public function addUserAccountTask(Request $request)
    {
        
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $task = $request->except(['member']);
            $members = $request->input('member');
            $data = Task::create($task);

            if($data){
                foreach ($members as $member) {
                    TaskMember::create([
                        'task_id' => $data->id,
                        'account_id' => $member['id'],
                       
                    ]);
                }
            }

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => 'success',
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 




    


    public function updateUserTask($task_id,Request $request)
    {
        
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = Task::find($task_id)->update($post);

            
            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => 'success',
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 


    public function addUserAccountTaskDiscussion(Request $request)
    {
        
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = TaskDiscussion::create($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => 'success',
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 


    public function addMenuAccess(Request $request)
    {
        
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = MenuAccess::create($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => 'success',
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 




    public function getUserAccountTaskDiscussion($account_id,$task_id)
    {
        $data = TaskDiscussion::where('task_id',$task_id)->With('sender')->orderBy('created_at', 'desc')->get();

        
           
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => ' $disc',
            'endpoint' => 'user_task_discussion',
            'data' =>   $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    //USER END



    //CREATOR START

    public function getCreatorDashboard(Request $request,$type)
    {

      

       
        if($type == 'total_creator'){
            $data = Creator::count();

        }
             
        if($type == 'total_management'){
            $data = CreatorManagement::count();

        }

        if($type == 'total_campaign'){
            $data = Campaign::count();

        }

        if($type == 'creator_tier'){
            // $data = Creator::get();
            // $counted = $data->countBy('location');
            // $data = $counted->all();
            $data = Creator::groupBy('tier')
                ->orderBy('total', 'DESC')
                ->selectRaw('count(*) as total, tier')
                
                ->get();

        }

        if($type == 'creator_location'){
            // $data = Creator::get();
            // $counted = $data->countBy('location');
            // $data = $counted->all();
            $data = Creator::groupBy('location')
            ->where('location','!=',null)
                ->orderBy('total', 'DESC')
                ->selectRaw('count(*) as total, location')
              
                ->get();

        }

        if($type == 'creator_campaign'){
            $data = [];
            
            // $data = Creator::get();
            // $data = $data->sortByDesc('campaign_count')->values()->all();

        }

        if($type == 'creator_management'){
            
            
            $data = CreatorManagement::get();
            $data = $data->sortByDesc('creator_count')->values()->all();

        }

        if($type == 'creator_persona'){
            
            
            $raw_data = Creator::get();
            $raw_data = $raw_data->pluck('persona')->values()->all();

            foreach($raw_data as $item){
                
                foreach (json_decode($item) as $child_item){
                    $collection[] = $child_item;
                }
            }
            $collection=  collect($collection);
            $data = $collection->countBy('name');

        }

        if($type == 'creator_age'){
            
            $data = array(
                [
                  'name' => 'Baby Boomers',
                  'range_age' => $this->_getRangeDate(1946,1964),
                  'count' => $this->_getAgeDemo(1946,1964),
                ],
                [
                  'name' => 'Generation X',
                  'range_age' => $this->_getRangeDate(1981,1996),
                  'count' => $this->_getAgeDemo(1981,1996),
                ],
                [
                  'name' => 'Generation Z',
                  'range_age' => $this->_getRangeDate(1997,2012),
                  'count' => $this->_getAgeDemo(1997,2012),
                ],
                [
                  'name' => 'Generation Alpha',
                  'range_age' => date("Y") - date(2013).' - Present',

                  'count' => $this->_getAgeDemo(2013,2100),
                ],
  
               );
        }





     
      
        
        
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),
            'data' =>  $data,
            'request' =>   $request->all(),
        );
       return Response::json($response);
    } 


    public function _getAgeDemo($from,$to){
        $data =  Creator::whereYear('dob','>=',$from)->
        whereYear('dob','<=',$to)->
        count();
        return $data;
    }

    public function _getRangeDate($from,$to){
       

        $from = date("Y") - date($from);
        $to = date("Y") - date($to);

        return $to.' - '.$from.' Years';
    }
    public function getCreator(Request $request)
    {

        $page = $request->input('page');

        $search = $request->input('search');
        $keywords = explode(' ', $search);
        $location = $request->input('location');
        $persona = $request->input('persona');
        $tier = $request->input('tier');
        $min_age = $request->input('min_age');
        $max_age = $request->input('max_age');
        $page = $request->input('page');
       

        // $data = Creator::With('auth');

       
        

        $data = Creator::With('auth')
         ->With('rate')
        // ->With('management')
       
        ->orderBy('created_at', 'desc');

      
            

        if($search){
            foreach($keywords as $word){
                $data->where(
                    function($query) use($word) {
                      return $query
                                ->WhereLike('first_name',$word)
                                ->OrWhereLike('last_name',$word);
                      
                     });
            
              
             }
           
        }
        if($persona){
            $data->WhereLike('persona',$persona);

        }
        if($location){
            $data->WhereLike('location',$location);

        }
        if($tier){
            $data->WhereLike('tier',$tier);

        }

       
        if($page){
            $data->forPage($page,25);

        }else{
            $page = 1;
            $data->forPage($page,25);

        }
    
        $result =  $data->get();

        
        if($min_age || $max_age){
            $flag = '1';
            $result =  $result
         
          ->whereBetween('age', [$min_age?$min_age:0, $max_age?$max_age:100])
          ->values()->all();
     
        }

      
       
          
        

        
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),
            'data' =>  $result,
            'page'=>  $page,

            'request' =>   $request->all(),
        );
       return Response::json($response);
    } 

    public function getManagement(Request $request)
    {

        $search = $request->input('search');
        $keywords = explode(' ', $search);
        $location = $request->input('location');
        $persona = $request->input('persona');
       
        // $data = Creator::With('auth');

       
        

        $data = CreatorManagement::With('creator')
        ->with('auth')
        ->orderBy('created_at', 'desc');
      
            

        if($search){
            foreach($keywords as $word){

                $data->where(
                    function($query) use($word) {
                      return $query
                                ->WhereLike('name',$word);
                              
                      
                     });
                
              
             }
           
        }
        // if($persona){
        //     $data->WhereLike('persona',$persona);

        // }
        // if($location){
        //     $data->WhereLike('location',$location);

        // }

        
        // $result =  $data->get()
        // ->where('performance_score','>',30);
    
        $result =  $data->get();

     
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),
            'data' =>  $result,
          
            'request' =>   $request->all(),
        );
       return Response::json($response);
    } 

    public function getManagementById($id)
    {
        // $data = Creator::where('id',$creator_id)
        //     ->With('rate')
        //     ->With('social')
        //     ->get();

              
            $data = CreatorManagement::With('creator.rate')
            //->with('creator.campaign.campaign')
            ->with('auth')

            ->find($id);
          
           
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => ' $disc',
            'endpoint' => 'user_task_discussion',
            'data' =>   $data,
            'request' =>  '',
        );
       return Response::json($response);
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => 'creator_person_by_id',
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function addManagement(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = CreatorManagement::create($post);
           

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);


        

      
    } 

    
    public function updateManagement($id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = CreatorManagement::find($id)->update($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);

        

      
    } 

    public function deleteManagement($id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            
            $data = CreatorManagement::find($id)->delete();

            if($data){
                 Creator::where('creator_management_id', '=', $id)->update(['creator_management_id' => null]);
                 ClientAuth::where('creator_management_id', '=', $id)->delete();

            }



            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);

        

      
    } 

    public function getCreatorCampaign()
    {
        $data = null;
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => 'creator_campaign_list',
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function getCreatorContract()
    {
        $data = null;
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => 'creator_contract_list',
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function getCreatorById($creator_id)
    {
        // $data = Creator::where('id',$creator_id)
        //     ->With('rate')
        //     ->With('social')
        //     ->get();

          $data = Creator::With('rate')
            ->With('social')
            ->With('performance')
            ->With('contact')
            ->With('auth')
            ->With('note.account')
        ->With('management')
           
            ->With('campaign.campaign')
            ->With('campaign.contract.scope')
          
            
         
            ->find($creator_id);
              
        
           
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => ' $disc',
            'endpoint' => 'user_task_discussion',
            'data' =>   $data,
            'request' =>  '',
        );
       return Response::json($response);
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => 'creator_person_by_id',
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function getCreatorPersonContact($person_id)
    {
        $data = null;
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => 'creator_person_contract',
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function getCreatorPersonContract($person_id)
    {
        $data = null;
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => 'creator_person_contract',
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function getCreatorPersonNote($person_id)
    {
        $data = null;
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => 'creator_person_note',
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 

   

  

    public function getCreatorPersonSocialMetric($person_id)
    {
        $data = SocialMetric::All();
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => 'creator_person_campaign',
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 
 

    public function addPerformance(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = Performance::create($post);

           

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);

    } 

    public function updatePerformance($performance_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            
              $data = Performance::find($performance_id)->update($post);
  
              $response = array(
                  'http_request' => 'POST',
                  'status_code' => 200,
                  'message_code' => $data,
                  'endpoint' => $request->url(),
                  'data' =>  $data,
                  'request' =>  $request->all(),
              );
              
          }
          else{
              $response = array(
                  'status_code' => 400,
                  'message_code' => 'not_allowed',
              );
        
          }
  
          return Response::json($response);

    } 



    public function addCreator(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = Creator::create($post);
                if($data){
                    $this->_addAccountActivity($request->input('create_by'),'Add New Creator','fs','');
                }
                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);

    } 

    public function deleteContact($contact_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = Contacts::find($contact_id)->delete();

            $response = array(
             
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);

       

    } 

    public function updateCreator($creator_id,Request $request)
    {
        
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
           
          
            $data = Creator::find($creator_id)->update($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 


    
    public function getCreatorPersonRateCart($person_id)
    {
        $data = null;
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => 'creator_person_campaign',
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function addCreatorRate($creator_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = RateCart::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);

    } 


    public function updateCreatorRate($rate_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = RateCart::find($rate_id)->update($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);

       

    } 

    public function deleteCreatorRate($rate_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = RateCart::find($rate_id)->delete();

            $response = array(
             
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);

       

    } 

    public function getCampaign(Request $request)
    {
        $search = $request->input('search');
        $client_name = $request->input('client');
        $keywords = explode(' ', $search);
        $page = $request->input('page');
        $data = Campaign::with('client')

        ->with('owner')
        ->orderBy('created_at', 'desc');
         
        if($search){
            foreach($keywords as $word){
                $data->WhereLike('campaign_name',$word);
                $data->OrWhereLike('campaign_name',$word);
                // $data->where(
                //     function($query) use($word) {
                //       return $query
                //                 ->WhereLike('campaign_name',$word)
                //                 ->OrWhereLike('campaign_name',$word);
                      
                //      });

                $data->orWhereHas('invited_creator.creator', function ($query) use($word){
                    $query->WhereLike('first_name', $word);
                    $query->OrWhereLike('last_name', $word);
                });
                
              
             }
           
        }

        if($client_name){
            $data->WhereHas('client', function ($query) use($client_name){
                $query->where('client_name', $client_name);
            });
        }
      
        if($page){
            $data->forPage($page,10);

        }else{
            $page = 1;
            $data->forPage($page,10);

        }

        $result =  $data->get();
              
        
           
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => ' $disc',
            'endpoint' => $request->url(),
            'data' =>   $result,
           
            'request' =>  '',
        );

        return Response::json($response);
    } 


    public function getCampaignById($campaign_id)
    {
       
        
        $data = Campaign::With('invited_creator.creator')
        ->With('invited_creator.creator.performance')
        ->With('invited_creator.contract.scope')
        ->With('client')
        ->With('owner')
        ->with('note.account')

        ->find($campaign_id);
         
           
              
        
           
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => ' $disc',
            'endpoint' => 'get_campaign',
            'data' =>   $data,
            'request' =>  '',
        );

        return Response::json($response);
    } 


    public function addCampaign(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = Campaign::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);

    } 


    public function updateCampaign($campaign_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = Campaign::find($campaign_id)->update($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);

       

    } 

    public function addInvitedCreator(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = CreatorCampaign::create($post);


            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);

       

    } 


    public function deleteInvitedCreator($campaign_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
         
            $data = CreatorCampaign::find($campaign_id)->delete();

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);

       

    } 


    public function addContract($creator_campaign_id, Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $contract = $request->except(['scope']);
                $scopes = $request->input('scope');
              
                //create contract
                
                $data = Contract::create($contract);
                
             //   $scope_model = ScopeWork::insert($scope);


                if($data->id){
                    //create scope of work
                    foreach ($scopes as $scope) {
                        ScopeWork::create([
                            'contract_id' => $data->id,
                            'type' => $scope['type'],
                            'name' => $scope['name'],
                            'platform_name' => $scope['platform_name'],
                         
                        ]);
                    }
                    $creator_campaign = CreatorCampaign::find($creator_campaign_id);
                    $creator_campaign->contract_id = $data->id;
                    $creator_campaign->save();
                }
               

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    // 'creator_campaign' =>  $creator_campaign,
                    'request' =>  $scopes,
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);

    } 

    public function getContract(Request $request)
    {
        $status = $request->input('status');
        $search = $request->input('search');
        $keywords = explode(' ', $search);

        $data = Contract::With('campaign')
        ->with('creator')
        ->With('scope')
        ->with('client')
        ->orderBy('created_at', 'desc');


        if($search){
            foreach($keywords as $word){
               
                $data->WhereHas('campaign', function ($query) use($word){
                    $query->WhereLike('campaign_name', $word);
                });

                $data->orWhereHas('creator', function ($query) use($word){
                    $query->WhereLike('first_name', $word);
                    $query->OrWhereLike('last_name', $word);
                });

                $data->orWhereHas('client', function ($query) use($word){
                    $query->WhereLike('client_name', $word);
                   // $query->OrWhereLike('last_name', $word);
                });


               
               
             }
           
        }

        

        if($status){
            $data->where('status',$status);

        }

        $result =  $data->get();
              
         
           
              
        
           
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => ' $disc',
            'endpoint' => $request->url(),
            'data' =>   $result,
            'request' =>  '',
        );

        return Response::json($response);

    } 


    public function updateContract($contract_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = Contract::find($contract_id)->update($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);

       

    } 



   
    //CREATOR END

   
    //CLIENT START
    public function getPost($status, Request $request)
    {
        $search = $request->input('search');
        $tab_view = $request->input('tab_view');

        $keywords = explode(' ', $search);
        $category = $request->input('category');
        $author_id = $request->input('author_id');
        $pinned_id = $request->input('pinned_id');
        $tab_view = $request->input('tab_view');
        $status = $request->input('status');


       // $status = $request->input('status');
       

       
        $data = NewsfeedPost::With('author')
        ->With('reaction')

        ->With('category.category')
        ->With('account')
        ->limit(8);
        $data->orderBy('created_at', 'desc');
        if($search){
            foreach($keywords as $word){
                $data->WhereLike('title',$word);
              

             }
           
        }

        if($status){
            $data->WhereLike('status',$status);

        }
        
        if($category){
          
            $data->WhereHas('category.category', function ($query) use($category){
                $query->where('name', $category);
            });
        }
        if($pinned_id){
            $data->WhereHas('reaction', function ($query) use($pinned_id){
                $query->where('newsfeed_account_id', $pinned_id);
            });
        }
        if($author_id){
            $data->where('author_id', $author_id);
        }

        if($status == 'publish'){
            $data->where('status', 'publish');
        }
    
        if($tab_view == 'trending'){
            $date = Carbon::now()->subDays(7);
            $data->where('created_at', '>=', $date);
        }

        $result =  $data->get();
  
        
     if($tab_view == 'trending'){
                $result =  $result->sortByDesc('like_count')
                ->sortByDesc('discussion_count')->values()->all();

    }
            


        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),

            'data' =>  $result,
           // 'request' =>  $tab_view,
        );
       return Response::json($response);
    } 

    public function getRecommendedPost(Request $request)
    {
      

       
        $data = NewsfeedPost::With('author')
        ->With('reaction')
        ->where('status', 'publish')
        ->where('is_featured', 1)
        
        ->With('account')
        ->limit(5);
       

        $result =  $data->get();
  

        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),

            'data' =>  $result,
           // 'request' =>  $tab_view,
        );
       return Response::json($response);
    } 


    public function addPost(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $category = $request->input('category');

                

                $body = $request->except(['category']);



                $data = NewsfeedPost::create($body);


                if($data->id){
                    if($category){
                          //create scope of work
                            foreach ($category as $item) {
                                NewsfeedPostCategory::create([
                                    'type'=>'category',
                                    'newsfeed_post_id' => $data->id,
                                    'newsfeed_category_id' => $item['id'],
                                
                                
                                ]);
                            }
                    }
                  
                   
                }
               

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    
    public function updatePost($post_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = NewsfeedPost::find($post_id)->update($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function getPostById($id)
    {
        $data = NewsfeedPost::With('author')
        ->With('account')
        ->With('category.category')
        ->With('tag.tag')
        ->With('reaction')

        ->With('discussion.account')
        ->With('discussion.author')

        ->With('brand.brand')

        ->find($id);
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => 'client_company',
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 
    
    public function getPostCategory($type,Request $request)
    {
        $search = $request->input('search');
        $keywords = explode(' ', $search);


       
        $data = NewsfeedCategory::Where('type',$type);
        if($search){
            foreach($keywords as $word){
                $data->WhereLike('name',$word);
               
             }
           
        }
        

        $result =  $data->get();

        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),

            'data' =>  $result,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function addPostCategory(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = NewsfeedCategory::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function addPostTerm(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = NewsfeedPostCategory::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function addReaction(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $name = $request->input('name');
                $author_id = $request->input('newsfeed_account_id');
                $post_id = $request->input('newsfeed_post_id');

                $reaction_id = $this->_checkReaction($author_id,$name,$post_id);
                if($reaction_id){
                    //remove
                    $data = $reaction_id;
                    $data = NewsfeedReaction::find($reaction_id)->delete();
                }
                else{
                    //add
                    $data = NewsfeedReaction::create($post);

                }
                
            // $post = array(
            //     'name' => 'like',
            //     'newsfeed_post_id' => 1,
             
            //     'newsfeed_account_id' => 1

            // );


                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function _checkReaction($author_id,$name,$post_id){
        $data = NewsfeedReaction::
        Where('newsfeed_account_id',$author_id)
        ->Where('name',$name)
        ->Where('newsfeed_post_id',$post_id)
        ->first();
        if($data){
            return $data->id;
        }else{
            return null;
        }
    }

    public function addPostDiscussion(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = NewsfeedDiscussion::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function removePostTerm($id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = NewsfeedPostCategory::Where('newsfeed_category_id',$id)->delete();

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 
    
    public function updatePostCategory($cat_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
             //   $data = NewsfeedCategory::create($post);
                $data = NewsfeedCategory::find($cat_id)->update($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function deletePostCategory($cat_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
             //   $data = NewsfeedCategory::create($post);
                $data = NewsfeedCategory::find($cat_id)->delete();

                if($data){
                    //remove relation
                     NewsfeedPostCategory::where('newsfeed_category_id',$cat_id)->delete();
                }

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 


    public function getPostKeywordFilter(Request $request)
    {
        $search = $request->input('search');
        $keywords = explode(' ', $search);


       
        $data = NewsfeedKeywordFilter::get();
        // if($search){
        //     foreach($keywords as $word){
        //         $data->WhereLike('client_name',$word);
        //         $data->orWhereHas('child', function ($query) use($word){
        //             $query->WhereLike('client_name',$word);
        //         });

        //      }
           
        // }
        

      //  $result =  $data->get();

        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),

            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function addPostKeywordFilter(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = NewsfeedKeywordFilter::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);


        

      
    } 

    public function updatePostKeywordFilter($note_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = NewsfeedKeywordFilter::find($note_id)->update($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);

        

      
    } 

    public function deleteKeywordFilter($note_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = NewsfeedKeywordFilter::find($note_id)->delete();

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);

        

      
    } 
    


    public function getNewsfeedAccount(Request $request)
    {
        $search = $request->input('search');
        $keywords = explode(' ', $search);


       
        $data = NewsfeedAccount::get()->orderBy('created_at', 'desc');
        // if($search){
        //     foreach($keywords as $word){
        //         $data->WhereLike('client_name',$word);
        //         $data->orWhereHas('child', function ($query) use($word){
        //             $query->WhereLike('client_name',$word);
        //         });

        //      }
           
        // }
        

      //  $result =  $data->get();

        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),

            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function getNewsfeedContact(Request $request)
    {
        $search = $request->input('search');
        $keywords = explode(' ', $search);


       
        $data = NewsfeedContactForm::get()->orderBy('created_at', 'desc');
        // if($search){
        //     foreach($keywords as $word){
        //         $data->WhereLike('client_name',$word);
        //         $data->orWhereHas('child', function ($query) use($word){
        //             $query->WhereLike('client_name',$word);
        //         });

        //      }
           
        // }
        

      //  $result =  $data->get();

        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),

            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function addNewsfeedContact(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = NewsfeedContactForm::create($post);
           

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);


        

      
    } 



        
    public function getPostTag(Request $request)
    {
        $search = $request->input('search');
        $keywords = explode(' ', $search);


       
        $data = NewsfeedTag::get();
        // if($search){
        //     foreach($keywords as $word){
        //         $data->WhereLike('client_name',$word);
        //         $data->orWhereHas('child', function ($query) use($word){
        //             $query->WhereLike('client_name',$word);
        //         });

        //      }
           
        // }
        

      //  $result =  $data->get();

        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),

            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 



    public function getClient($type,Request $request)
    {
        $search = $request->input('search');
        $category = $request->input('category');
        $keywords = explode(' ', $search);


        if ($request->has('client_type')) {
            $client_type = $request->input('client_type');
         }
        $data = Client::With('child.owner')
        ->With('child.contact')
        ->With('owner')

      //  ->With('note.account')
         
        ->where('client_type',$type)
        ->orderBy('created_at', 'desc');

        if($search){
            foreach($keywords as $word){
                $data->WhereLike('client_name',$word);
                $data->orWhereHas('child', function ($query) use($word){
                    $query->WhereLike('client_name',$word);
                });

             }
           
        }

        if($category){
            $data->WhereLike('client_category',$category);

        }
        

        $result =  $data->get();

        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),

            'data' =>  $result,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function getClientById()
    {
        $data = Client::With('child')->where('client_type','company')->get();
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => 'client_company',
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function addClient(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = Client::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    
    public function addNewsfeedPost(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = NewsfeedPost::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 


   public function updateClient($client_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = Client::find($client_id)->update($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function deletClient()
    {
        $data = Client::With('child')->where('client_type','company')->get();
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => 'client_company',
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 



    public function addAquisition(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = ClientAquisition::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function getAquisition(Request $request)

    
    {
        $search = $request->input('search');
        $keywords = explode(' ', $search);
        $page = $request->input('page');

        $stages = $request->input('stages');
        $client = $request->input('client');

        $periode_start = $request->input('periode');
        $periode_end = $request->input('periode_end');


        $data = ClientAquisition::With('client')
        ->With('owner')
        ->orderBy('created_at', 'desc');

        if($periode_start){
            $data->where('periode', '>=', $periode_start);

        }
        if($periode_end){
            $data->orwhere('periode_end', '<=', $periode_end);
    
        }

             

        if($search){
            foreach($keywords as $word){

                $data->where(
                    function($query) use($word) {
                      return $query
                                ->WhereLike('aquisition_name',$word);
                               
                      
                     });
                
              
             }
        }

        if($stages){
            $data->WhereLike('deal_stage',$stages);

        }

        if($client){
            $data->WhereHas('client', function ($query) use($client){
                $query->where('client_name', $client);
            });
        }
      
        if($page){
            $data->forPage($page,25);

        }else{
            $page = 1;
            $data->forPage($page,25);

        }
        $result =  $data->get();


        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),

            'data' =>  $result,
            'request' =>   $request->all(),

        );
       return Response::json($response);
    } 

    public function getAquisitionById($aquisition_id)
    {
       
        
        $data = ClientAquisition::With('client')
        ->With('client.contact')
        ->With('note.account')

        ->With('owner')
        ->With('scope')
        ->find($aquisition_id);
         
           
              
        
           
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => ' $disc',
            'endpoint' => 'get_campaign',
            'data' =>   $data,
            'request' =>  '',
        );

        return Response::json($response);
    } 


    public function updateAquisition($aquisition_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = ClientAquisition::find($aquisition_id)->update($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 


    public function addScope(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = ScopeWork::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function getScope()
    {
        $data = ScopeWork::All();
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => 'client_company',
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function updateScope($scope_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = ScopeWork::find($scope_id)->update($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

     //CLIENT END

       //CHANNEL START
    public function getVendor($type,Request $request)
    {

        $rating = $request->input('rating');

        $search = $request->input('search');
        $category = $request->input('category');

        $keywords = explode(' ', $search);
       
        $data = Vendor::With('product.category')
        ->With('contact')
        ->With('category')
        ->With('auth')
        ->where('type',$type)
        ->orderBy('created_at', 'desc');

        if($search){
            foreach($keywords as $word){

                $data->where(
                    function($query) use($word) {
                      return $query
                                ->WhereLike('name',$word);
                           //     ->OrWhereLike('last_name',$word);
                      
                     });
                
              
             }
           
        }


        if($rating){
            $data->WhereLike('rating_name',$rating);

        }



        if($category){
            $data->WhereHas('category', function ($query) use($category){
                $query->where('name', $category);
              
            });
        }



     

        $result =  $data->get();



        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),
            'data' =>  $result,
            'request' =>  '',
        );

        
       return Response::json($response);
    } 

    public function getVendorById($vendor_id,Request $request)
    {
       
        $data = Vendor::With('product.category')
        ->With('contact')
        ->With('auth')
        ->With('category')
        ->find($vendor_id);


        
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 



    public function addVendor(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = Vendor::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function deleteVendor($id,Request $request)
    {
        
        $data = Vendor::find($id)->delete();

        VendorProduct::where('vendor_id', '=', $id)->delete();
       // VendorProductCategory::where('vendor_id', '=', $id)->delete();


        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function updateVendor($vendor_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = Vendor::find($vendor_id)->update($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function getVendorProductCategory($type,Request $request)
    {   $search = $request->input('search');
        $keywords = explode(' ', $search);
      
        $data = VendorProductCategory::where('type',$type)->orderBy('created_at', 'desc');


        if($search){
            foreach($keywords as $word){

                $data->where(
                    function($query) use($word) {
                      return $query
                                ->WhereLike('name',$word);
                            
                      
                     });
                
              
             }
           
        }

        
      

        $result =  $data->get();

        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),
            'data' =>  $result,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function getVendorProduct($type,Request $request)
    {
        $search = $request->input('search');
        $category = $request->input('category');

        $keywords = explode(' ', $search);
        $data = VendorProduct::where('type',$type)->with('category')
        ->with('vendor')
        ->orderBy('created_at', 'desc');

        
        if($search){
            foreach($keywords as $word){

                $data->where(
                    function($query) use($word) {
                      return $query
                                ->WhereLike('name',$word);
                             //   ->OrWhereLike('last_name',$word);
                      
                     });
                
              
             }
           
        }

        if($category){
            $data->WhereHas('category', function ($query) use($category){
                $query->where('name', $category);
              
            });
        }


        $result =  $data->get();

        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),
            'data' =>  $result,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function addVendorProductCategory(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = VendorProductCategory::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function updateVendorProductCategory($category_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = VendorProductCategory::find($category_id)->update($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function deleteVendorProductCategory($category_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = VendorProductCategory::find($category_id)->delete();

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 
    
    

    public function deleteVendorProduct($product_id,Request $request)
    {
        
        $data = VendorProduct::find($product_id)->delete();

        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 


    public function addVendorProduct(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = VendorProduct::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function updateVendorProduct($product_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = VendorProduct::find($product_id)->update($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 
    


    public function getInventory($type,Request $request)
    {
        $search = $request->input('search');
        $keywords = explode(' ', $search);
        $category = $request->input('category');
        $status = $request->input('status');
        $page = $request->input('page');


        $data = Inventory::With('stock')
        ->with('category')
        ->where('type',$type);

        
        if($search){
            foreach($keywords as $word){

                $data->where(
                    function($query) use($word) {
                      return $query
                                ->WhereLike('name',$word);
                            //    ->OrWhereLike('last_name',$word);
                      
                     });
                
              
             }
           
        }


        if($category){
            $data->WhereHas('category', function ($query) use($category){
                $query->where('name', $category);
              
            });
        }

        
        if($status){
            $data->WhereLike('status',$status);

        }


        if($page){
            $data->forPage($page,25);

        }else{
            $page = 1;
            $data->forPage($page,25);

        }

        $result =  $data->get();

        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),
            'data' =>  $result,
            'request' =>  '',
        );
       return Response::json($response);
    } 


    public function addInventory(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = Inventory::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function deleteInventory($type,$id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = Inventory::find($id)->delete();

           // $data = InventoryStock::create($post);
           if($type == 'product'){
           InventoryStock::where('inventory_id', '=', $id)->delete();

           }


            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function addInventoryStock($inventory_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = InventoryStock::create($post);

               
                $inv = Inventory::find($inventory_id);
                $inv->last_stock_in = date('Y-m-d H:i:s');
                $inv->save();

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function updateInventory($inventory_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = Inventory::find($inventory_id)->update($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function updateInventoryStock($inventory_id,$stock_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = InventoryStock::find($stock_id)->update($post);
            if ($request->has('stock_out')) {
                $inventory = Inventory::find($inventory_id);
                $inventory->last_stock_out = date('Y-m-d H:i:s');
                $inventory->save();
             }
            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 
   


    public function getEmployee(Request $request)
    {
        $status = $request->input('status');
        $position = $request->input('division');
        $education = $request->input('education');
        $page = $request->input('page');

        $search = $request->input('search');
        $keywords = explode(' ', $search);
       
        $data = Employee::With('education')
        ->With('experience')
        ->With('performance')->orderBy('created_at', 'desc');

        if($search){
            foreach($keywords as $word){

                $data->where(
                    function($query) use($word) {
                      return $query
                                ->WhereLike('first_name',$word)
                                ->OrWhereLike('last_name',$word);
                      
                     });

                  
                
              
             }
           
        }
        if($status){
            $data->WhereLike('status',$status);

        }
        if($position){
            $data->WhereLike('position',$position);

        }


        if($education){
            $data->orWhereHas('education', function ($query) use($education){
                $query->WhereLike('education_level', $education);
              
            });
        }
        


        if($page){
            $data->forPage($page,25);

        }else{
            $page = 1;
            $data->forPage($page,25);

        }
    
        $result =  $data->get();

        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),
            'data' =>  $result,
            'request' =>  '',
        );
       return Response::json($response);
    } 


    public function getEmployeeById($employee_id,Request $request)
    {
       
        $data = Employee::With('education')
        ->With('experience')
        ->With('note.account')

        ->With('performance')
        ->find($employee_id);
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 


    public function addEmployee(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = Employee::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 


    public function updateEmployee($employee_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = Employee::find($employee_id)->update($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 


    public function addEmployeeEducation(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = EmployeeEducation::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function deleteEmployeeEducation($education_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = EmployeeEducation::find($education_id)->delete();

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function addEmployeeExperience(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = EmployeeExperience::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function deleteEmployeeExperience($experience_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = EmployeeExperience::find($experience_id)->delete();

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function addEmployeePerformance(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = EmployeePerformance::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function updateEmployeePerformance($performance_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = EmployeePerformance::find($performance_id)->update($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 


    //community start

    public function getCommunity(Request $request)
    {
       
        $data = Community::get();
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 
    
    public function getCommunityMember(Request $request)
    {
        $status = $request->input('status');
        $page = $request->input('page');

        $search = $request->input('search');
        $keywords = explode(' ', $search);
        $location = $request->input('Domicile');
       
        $data = CommunityMember::With('community')
        ->With('note.account')
      

        ->With('campaign')
        ->With('auth')->orderBy('created_at', 'desc');

        if($search){
            foreach($keywords as $word){

                $data->where(
                    function($query) use($word) {
                      return $query
                                ->WhereLike('first_name',$word)
                                ->OrWhereLike('last_name',$word);
                      
                     });
                
              
             }
           

        }

        if($status){
            $data->where('status',$status);

        }

        if($location){
            $data->WhereLike('location',$location);

        }

        if($page){
            $data->forPage($page,25);

        }else{
            $page = 1;
            $data->forPage($page,25);

        }

        $result =  $data->get();
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),
            'data' =>  $result,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function getCommunityMemberById($member_id,Request $request)
    {
        $search = $request->input('search');
       
        $data = CommunityMember::With('community')
        ->With('points.points')
        ->With('reward.reward')
        ->With('note.account')
        ->With('auth')
        ->With('campaign.campaign')
        ->find($member_id);

        

        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function addCommunityMember(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $form = $request->except(['password','confirm_password']);


                $data = CommunityMember::create($form);

                if($data){
                    if($data->password_hash){
                        $auth = array(
                            'username'=> $data->email,
                            'is_user_generate'=> true,
                            'password'=> $data->password_hash,
                            'community_member_id'=> $data->id,
        
                        );
        
                        $auth_cr = ClientAuth::create($auth);
                    }  else{
                        $auth_cr =null;
                    } 
                   
                }
               

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'auth'=>$auth_cr,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function updateCommunityMember($member_id,Request $request)
    {
       

        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = CommunityMember::find($member_id)->update($post);

            $status = $request->input('status');
            if($status == 'active'){
               //add referal point 
               $member = CommunityMember::find($member_id);
               $referal_member_id = $member->referal_code;
               if($referal_member_id){
                $this->_addReferalPoints($referal_member_id,$member_id);
                
               }

            }

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function _addReferalPoints($referal_member_id,$member_id){
        //check customer id
        $member = CommunityMember::find($referal_member_id);


        if($member){
            $referal_flag = $referal_member_id.$member_id;
            $data = CommunityPoints::where('referal_flag',$referal_flag)->first();
            if(!$data){
                $post = array(
                    'type' => 'credit',
                    'community_member_id' => $referal_member_id,
                    'value'=>5,
                    'referal_flag'=>$referal_flag,
    
                    'description'=>'referal points from member:'.$member_id
                 );
                 $points = CommunityPoints::create($post);
            }
            
        }
       
    }

    public function getCommunityCampaign(Request $request)
    {
        $status = $request->input('status');
        $community_id = $request->input('community_id');

        $search = $request->input('search');
        $keywords = explode(' ', $search);
       
        $data = CommunityCampaign::With('community')
        ->With('participant')->orderBy('created_at', 'desc');

        if($search){
            foreach($keywords as $word){
                $data->WhereLike('campaign_name',$word);

                $data->orWhereHas('participant.member', function ($query) use($word){
                    $query->WhereLike('first_name', $word);
                    $query->OrWhereLike('last_name', $word);
                });

               // $data->OrWhereLike('campaign_name',$word);
              
             }
           
        }


        
        if($status){
            $data->WhereLike('status',$status);

        }
        if($community_id){
            $data->where('community_id',$community_id);
            $data->orwhere('community_id','0');

        }

      

        $result =  $data->get();

        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),
            'data' =>  $result,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function getCommunityCampaignById($member_id,Request $request)
    {
       
        $data = CommunityCampaign::With('community')
        ->With('note.account')
        ->With('participant.member')
        ->find($member_id);
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function addCommunityCampaign(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = CommunityCampaign::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 


    public function addCommunityCampaignMember($campaign_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = CommunityCampaignMember::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function updateCommunityCampaignMember($member_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = CommunityCampaignMember::find($member_id)->update($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 



    public function updateCommunityCampaign($campaign_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = CommunityCampaign::find($campaign_id)->update($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 
    

    public function getPointsLeaderboard(Request $request)
    {
       
        $data = CommunityMember::get();
       $sorted = $data->sortByDesc('total_credit'); 
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),
            'data' => $sorted->values()->all(),
          
            
            'request' =>  '',
        );
       return Response::json($response);
    } 

    
    public function getCommunityPoints(Request $request)
    {
       
        $data = CommunityPoints::With('member')
        ->With('points')->orderBy('created_at', 'desc')
        ->get();
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function getCommunityPointsCollection(Request $request)
    {
       
        $data = CommunityPointsCollection::orderBy('created_at', 'desc')->get();
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 



    public function addCommunityPointsCollection(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = CommunityPointsCollection::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

      public function updateCommunityPointsCollection($collection_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = CommunityPointsCollection::find($collection_id)->update($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    

       
    public function getReward(Request $request)
    {
       
        $data = CommunityReward::orderBy('created_at', 'desc')->get();
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 


    public function addReward(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = CommunityReward::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 
    


       
    public function getRewardRedeem(Request $request)
    {
        $search = $request->input('search');
        $keywords = explode(' ', $search);

        $status = $request->input('status');

        $data = CommunityRewardRedeem::With('member')
        ->With('reward')->orderBy('created_at', 'desc');
        
 

        if($search){
            foreach($keywords as $word){
                $data->WhereLike('name',$word);

                $data->orWhereHas('member', function ($query) use($word){
                    $query->WhereLike('first_name', $word);
                    $query->OrWhereLike('last_name', $word);
                });

               // $data->OrWhereLike('campaign_name',$word);
              
             }
           
        }

        if($status){
            $data->where('status',$status);

          }
          $result =  $data->get();

        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),
            'data' =>  $result,
           'request' =>  $request->all(),
        );
       return Response::json($response);
    } 


    public function updateRewardRedeem($redeem_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = CommunityRewardRedeem::find($redeem_id)->update($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    
    public function addMemberPoints(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = CommunityPoints::create($post);

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    
    public function updateMemberPoints($id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = CommunityPoints::find($id)->update($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

        
    public function addCampaignPoints($member_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = CommunityPoints::create($post);

                if($data->id){
                    $obj = CommunityCampaignMember::find($member_id);
                    $obj->points_activity_id = $data->id;
                    $obj->save();
                }

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function addCustomMemberReward(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = CommunityRewardRedeem::create($post);


               

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function addMemberReward($member_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = CommunityRewardRedeem::create($post);


                if($data->id){
                    $obj = CommunityCampaignMember::find($member_id);
                    $obj->reward_redeem_id = $data->id;
                    $obj->save();
                }
               

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 


    public function addMemberRedeem($reward_id,$member_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {
                $post = $request->all();
                    
                //check reward avail
                $reward = CommunityReward::find($reward_id);
                $require_points = $reward->redeem_points;
                $total_redeem = $reward->total_redeem;
                $quota = $reward->quota;
                if($quota > $total_redeem){

                       //check member points
                        $member = CommunityMember::find($member_id);
                        $available_points = $member->total_points;

                        //reduce member points
                        if($available_points>=$require_points){
                            $desc = 'exchange points with '.$reward->name;
                            $res = $this->_reduceMemberPoints($require_points,$member_id,$desc);
                            $data = CommunityRewardRedeem::create($post);  
                       
                            $response = array(
                                'http_request' => 'POST',
                                'status_code' => 200,
                                'message_code' => 'reduce points'.$res,
                                'endpoint' => $request->url(),
                                'data' =>   $data,
                                'request' =>  $request->all(),
                            );
            
                        }else{
                              
                            $response = array(
                                'status_code' => 400,
                                'message_code' => 'Member Points Not Enough!',
                            );
                        }

                      
                    

                }else{
                     
                    $response = array(
                        'status_code' => 400,
                        'message_code' => 'Reward Quota Exceeded!',
                    );
                }

             
        
               

              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    private function _reduceMemberPoints($value,$member_id,$desc){
        $post = array(
           'type' => 'debit',
           'description' => $desc,
           'community_member_id'=> $member_id,
           'value' => $value,
          
        );
        $data = CommunityPoints::create($post);
        if($data->id){
            return true;
        }
    }


    public function getTicket(Request $request)
    {
       
        $data = CommunityTicket::With('member')->orderBy('created_at', 'desc')->get();
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 


    public function getTicketByMemberId($member_id, Request $request)
    {
        $data = CommunityTicket::With('member')
        ->where('community_member_id', $member_id)
        ->get();
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    public function getTicketById($ticket_id,Request $request)
    {
       
        $data = CommunityTicket::With('discussion.member')
         ->With('member.points')
         ->With('member.reward')
        ->With('member.campaign.campaign')
     
        ->With('discussion.account')
        ->With('member')
        ->find($ticket_id);
        $response = array(
            'http_request' => 'GET',
            'status_code' => 200,
            'message_code' => '',
            'endpoint' => $request->url(),
            'data' =>  $data,
            'request' =>  '',
        );
       return Response::json($response);
    } 

    


    public function addTicketDiscussion(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = CommunityTicketDiscussion::create($post);


               

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 

    public function addTicket(Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            try {

                $post = $request->all();
                $data = CommunityTicket::create($post);


               

                $response = array(
                    'http_request' => 'POST',
                    'status_code' => 200,
                    'message_code' => 'success',
                    'endpoint' => $request->url(),
                    'data' =>  $data,
                    'request' =>  $request->all(),
                );
              
              } catch (\Exception $e) {
                $response = array(
                    'status_code' => 400,
                    'message_code' =>  $message_code =  $e->getMessage(),
                );
              }
          
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 


    
    public function updateTicket($ticket_id,Request $request)
    {
        if($this->_isValidated($request->bearerToken())){
            $post = $request->all();
            $data = CommunityTicket::find($ticket_id)->update($post);

            $response = array(
                'http_request' => 'POST',
                'status_code' => 200,
                'message_code' => $data,
                'endpoint' => $request->url(),
                'data' =>  $data,
                'request' =>  $request->all(),
            );
            
        }
        else{
            $response = array(
                'status_code' => 400,
                'message_code' => 'not_allowed',
            );
      
        }

        return Response::json($response);
    } 


    //community end


}