<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\hasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


class CommunityTicket extends Model
{protected $guarded = [];  
    use HasFactory;

    function discussion(): HasMany {
        return $this->hasMany(CommunityTicketDiscussion::class);
    }
     
    function member(): BelongsTo {
        return $this->belongsTo(CommunityMember::class,'community_member_id');
    }


}
