<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Relations\hasMany;

class Community extends Model
{
    protected $guarded = [];  
    protected $appends = [
        'total_member','total_campaign'
    ];
    use HasFactory;

    function member(): HasMany {
        return $this->hasMany(CommunityMember::class);
    }
    function campaign(): HasMany {
        return $this->hasMany(CommunityCampaign::class);
    }

    public function getTotalMemberAttribute()
    {
        
        $total = 0;
        $total = $this->member()->count();
        return $total;
       
    }
    public function getTotalCampaignAttribute()
    {
        
        $total = 0;
        $total = $this->member()->count();
        return $total;
       
    }

}
