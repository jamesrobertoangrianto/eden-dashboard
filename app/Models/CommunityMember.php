<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

use Illuminate\Database\Eloquent\Relations\hasMany;
use Illuminate\Database\Eloquent\Relations\hasOne;
class CommunityMember extends Model
{
    protected $guarded = [];  
    protected $appends = [
        'total_points','total_credit','total_debit','community_name'
    ];
    use HasFactory;

    function community(): BelongsTo {
        return $this->belongsTo(Community::class);
    }


    function note(): HasMany {
        return $this->hasMany(Note::class);
    }

    
    function points(): HasMany {
        return $this->hasMany(CommunityPoints::class);
    }

    function reward(): HasMany {
        return $this->hasMany(CommunityRewardRedeem::class);
    }



    function auth(): hasOne {
        return $this->hasOne(ClientAuth::class,'community_member_id');
    }


    function campaign(): HasMany {
        return $this->hasMany(CommunityCampaignMember::class);
    }

    public function getTotalPointsAttribute()
    {
        
        $total = 0;
        $credit = $this->points()->where('type','credit')->get();
        $debit = $this->points()->where('type','debit')->get();
        $total = $credit->sum('value') - $debit->sum('value');
        return $total;
       
       
    }

    public function getTotalCreditAttribute()
    {
        
        $total = 0;
        $credit = $this->points()->where('type','credit')->get();
        $total = $credit->sum('value');
        return $total;
       
       
    }

    public function getCommunityNameAttribute()
    {
        
       
        $community_id = $this->community_id;
        if($community_id == 1){
            $name = 'Mak Pintar';
        }elseif($community_id == 2){
            $name = 'Pekerja Pintar';

        }elseif($community_id == 3){
            $name = 'General';
        }
        else{
            $name = null;
        }
       
        return $name;
       
       
    }

    public function getTotalDebitAttribute()
    {
        
        $total = 0;
        
        $debit = $this->points()->where('type','debit')->get();
        $total = $debit->sum('value');
        return $total;
       
       
    }



    public function test()
    {
        
        $total = 0;
        $credit = $this->points()->where('type','credit')->get();
        $debit = $this->points()->where('type','debit')->get();
        $total = $credit->sum('value') - $debit->sum('value');
        return $total;
       
       
    }


    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'like', '%'.$value.'%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'like', '%'.$value.'%');
    }


}
