<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
class CommunityRewardRedeem extends Model
{ protected $guarded = [];  
    use HasFactory;

    function member(): BelongsTo {
        return $this->belongsTo(CommunityMember::class,'community_member_id');
    }

    function reward(): BelongsTo {
        return $this->belongsTo(CommunityReward::class,'community_reward_id');
    }

    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'like', '%'.$value.'%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'like', '%'.$value.'%');
    }


}
