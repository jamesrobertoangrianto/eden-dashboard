<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\hasMany;
class Contract extends Model
{ 
    protected $guarded = [];  
    
    use HasFactory;

    function campaign(): BelongsTo {
        return $this->belongsTo(Campaign::class,'campaign_id');
    }
    function creator(): BelongsTo {
        return $this->belongsTo(Creator::class,'creator_id');
    }

    function client(): BelongsTo {
        return $this->belongsTo(Client::class,'client_id');
    }

    function scope(): hasMany {
        return $this->hasMany(ScopeWork::class);
    }

    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'like', '%'.$value.'%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'like', '%'.$value.'%');
    }

}
