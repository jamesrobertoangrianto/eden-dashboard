<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CommunityTicketDiscussion extends Model
{protected $guarded = [];  
    use HasFactory;

    function member(): BelongsTo {
        return $this->belongsTo(CommunityMember::class,'community_member_id');
    }
    function account(): BelongsTo {
        return $this->belongsTo(Account::class,'account_id');
    }
    
}
