<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\hasMany;


class ClientAquisition extends Model
{
    protected $guarded = []; 
    protected $appends = [
        'scope_count'
    ];
    use HasFactory;

    function owner(): BelongsTo {
        return $this->belongsTo(Account::class,'owner_id');
    }

    function note(): HasMany {
        return $this->hasMany(Note::class);
    }


    function client(): BelongsTo {
        return $this->belongsTo(Client::class,'client_id');
    }

    function scope(): hasMany {
        return $this->hasMany(ScopeWork::class);
    }



    public function getScopeCountAttribute()
    {
       return $this->scope()->count();
    }


    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'like', '%'.$value.'%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'like', '%'.$value.'%');
    }

  


}
