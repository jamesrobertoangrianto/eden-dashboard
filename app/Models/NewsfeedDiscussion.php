<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


class NewsfeedDiscussion extends Model
{
    protected $guarded = [];  

    use HasFactory;

    function account(): BelongsTo {
        return $this->belongsTo(Account::class,'account_id');
    }

    function author(): BelongsTo {
        return $this->belongsTo(NewsfeedAccount::class,'newsfeed_account_id');
    }

}
