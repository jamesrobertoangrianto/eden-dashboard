<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

use Illuminate\Database\Eloquent\Relations\hasMany;
class CreatorManagement extends Model
{
    protected $guarded = [];  
    protected $appends = [
        'creator_count'
    ];
    use HasFactory;


    function creator(): HasMany {
        return $this->hasMany(Creator::class,'creator_management_id');
    }

    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'like', '%'.$value.'%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'like', '%'.$value.'%');
    }


    public function getCreatorCountAttribute()
    {
     
       return $this->creator()->count();
    }


    function auth(): hasOne {
        return $this->hasOne(ClientAuth::class);
    }


}
