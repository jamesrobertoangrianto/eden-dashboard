<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Relations\HasMany;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;


class Vendor extends Model
{   
    protected $guarded = [];  
    protected $appends = [
        'product_count'
    ];
    use HasFactory;

    function product(): HasMany {
        return $this->hasMany(VendorProduct::class);
    }

    function contact(): hasMany {
        return $this->hasMany(Contacts::class);
    }

    function category(): BelongsTo {
        return $this->belongsTo(VendorProductCategory::class,'category_id');
    }

    function auth(): hasOne {
        return $this->hasOne(ClientAuth::class,'vendor_id');
    }


    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'like', '%'.$value.'%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'like', '%'.$value.'%');
    }
    
    public function getProductCountAttribute()
    {
     
       return $this->product()->count();
    }

}