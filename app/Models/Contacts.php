<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\hasMany;

class Contacts extends Model
{
    use HasFactory;
    protected $guarded = [];  
    
    function creator(): BelongsTo {
        return $this->belongsTo(Creator::class);
    }
    
    function client(): BelongsTo {
        return $this->belongsTo(Client::class);
    }
    
}
