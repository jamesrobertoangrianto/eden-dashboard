<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CreatorCampaign extends Model
{
    protected $guarded = [];  

    protected $appends = [
        'contracts'
    ];
    use HasFactory;


    public function getContractsAttribute()
    {
        $data = Contract::orderBy('created_at', 'desc')->get();
       return $data;
    }

    function creator(): BelongsTo {
        return $this->belongsTo(Creator::class,'creator_id');
    }

    function contract(): BelongsTo {
        return $this->belongsTo(Contract::class,'contract_id');
    }
  

    function campaign(): BelongsTo {
        return $this->belongsTo(Campaign::class,'campaign_id');
    }


    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'like', '%'.$value.'%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'like', '%'.$value.'%');
    }



}
