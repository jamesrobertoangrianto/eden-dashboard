<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use Illuminate\Database\Eloquent\Relations\HasMany;
class Inventory extends Model
{
    protected $guarded = [];  
    use HasFactory;

    function stock(): HasMany {
        return $this->hasMany(InventoryStock::class);
    }

    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'like', '%'.$value.'%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'like', '%'.$value.'%');
    }

    function category(): BelongsTo {
        return $this->belongsTo(VendorProductCategory::class,'category_id');
    }
}
