<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Task extends Model
{
    
    protected $guarded = [];  
    protected $appends = [
        'total_discussion'
    ];
    use HasFactory;

    
    function discussion(): HasMany {
        return $this->hasMany(TaskDiscussion::class);
    }
    function member(): HasMany {
        return $this->hasMany(TaskMember::class);
    }

    function owner(): BelongsTo {
        return $this->belongsTo(Account::class, 'owner_id');
    }

    public function getTotalDiscussionAttribute()
    {
     
       return $this->discussion()->count();
    }

    public function scopePopular(Builder $query): void
    {
       
        $query->WhereHas('member', function ($query) {
                $query->where('account_id', 1);
            });
    }


    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'like', '%'.$value.'%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'like', '%'.$value.'%');
    }

    
 
   
}
