<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Client extends Model
{
    protected $guarded = [];  
    protected $appends = [
        'campaign_count','total_aquisition'
    ];
    use HasFactory;

    function child(): HasMany {
        return $this->hasMany(Client::class,'parrent_id');
    }

    function campaign(): hasMany {
        return $this->hasMany(Campaign::class);
    }

    function owner(): BelongsTo {
        return $this->belongsTo(Account::class,'owner_id');
    }



    public function getCampaignCountAttribute()
    {
     
       return $this->campaign()->count();
    }

    public function getTotalAquisitionAttribute()
    {
        
        $total = 0;
        $aquisition = $this->aquisition()->where('deal_stage','won')->get();
        $total = $aquisition->sum('budget');
        return $total;
       
       
    }

    public function scopeTotal()
    {
        
       return $this->campaign();
       
    }


    function contact(): hasMany {
        return $this->hasMany(Contacts::class);
    }

    function aquisition(): hasMany {
        return $this->hasMany(ClientAquisition::class);
    }

    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'like', '%'.$value.'%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'like', '%'.$value.'%');
    }
    
}
