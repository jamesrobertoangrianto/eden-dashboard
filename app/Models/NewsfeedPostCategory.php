<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


class NewsfeedPostCategory extends Model
{
    protected $guarded = [];  

    
    use HasFactory;


    function brand(): BelongsTo {
        return $this->belongsTo(NewsfeedCategory::class,'newsfeed_category_id');
    }
    function tag(): BelongsTo {
        return $this->belongsTo(NewsfeedCategory::class,'newsfeed_category_id');
    }
    function category(): BelongsTo {
        return $this->belongsTo(NewsfeedCategory::class,'newsfeed_category_id');
    }
}
