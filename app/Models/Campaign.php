<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
class Campaign extends Model
{ 
    protected $guarded = [];  
    // protected $appends = [
    //     'scope_count'
    // ];
    use HasFactory;
    

    function invited_creator(): HasMany {
        return $this->hasMany(CreatorCampaign::class);
    }



    function note(): HasMany {
        return $this->hasMany(Note::class);
    }


    function client(): BelongsTo {
        return $this->belongsTo(Client::class);
    }

    
    function owner(): BelongsTo {
        return $this->belongsTo(Account::class,'owner_id');
    }


    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'like', '%'.$value.'%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'like', '%'.$value.'%');
    }
   
    // public function getScopeCountAttribute()
    // {
    //    return $this->client()->first()->client_name;
    // }
   

    

   

}
