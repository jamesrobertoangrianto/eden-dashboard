<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Creator extends Model
{
    protected $guarded = [];  
    protected $appends = [
        'campaign_count','performance_score','age'
    ];
    use HasFactory;

    function social(): HasMany {
        return $this->hasMany(SocialMetric::class,'creator_id');
    }
    function rate(): HasMany {
        return $this->hasMany(RateCart::class,'creator_id');
    }
    function campaign(): HasMany {
        return $this->hasMany(CreatorCampaign::class,'creator_id');
    }

    public function getCampaignCountAttribute()
    {
     
       return $this->campaign()->count();
    }

    public function getAgeAttribute()
    {
        if($this->dob){
         $age = date("Y") - date('Y', strtotime($this->dob))  ;

        }
        else{
            $age = 0;
        }
       return $age;
    }


    public function getPerformanceScoreAttribute()
    {
        
        $total = 0;
        $performance = $this->performance()->get();
        $total = $performance->sum('performance_score') / 3;
        return $total;
       
      
       
    }

    function note(): HasMany {
        return $this->hasMany(Note::class);
    }



    function auth(): hasOne {
        return $this->hasOne(ClientAuth::class);
    }


    function management(): BelongsTo {
        return $this->BelongsTo(CreatorManagement::class,'creator_management_id');
    }

    function performance(): HasMany {
        return $this->hasMany(Performance::class,'creator_id');
    }

    function contact(): hasMany {
        return $this->hasMany(Contacts::class,'creator_id');
    }

    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'like', '%'.$value.'%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'like', '%'.$value.'%');
    }
}
