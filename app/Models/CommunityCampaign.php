<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\hasMany;

class CommunityCampaign extends Model
{
    protected $guarded = [];  
    use HasFactory;

    protected $appends = [
        'community_name',
    ];

    function community(): BelongsTo {
        return $this->belongsTo(Community::class);
    }

    function participant(): HasMany {
        return $this->hasMany(CommunityCampaignMember::class);
    }

    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'like', '%'.$value.'%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'like', '%'.$value.'%');
    }

    function note(): HasMany {
        return $this->hasMany(Note::class);
    }


    public function getCommunityNameAttribute()
    {
        if($this->community_id == '0'){
            $name = 'All Community';

        }
        
        else if($this->community_id == '1'){
            $name = 'Mak Pintar';
   
           }
        else if($this->community_id == '2'){
            $name = 'Pekerja Pintar';
        }
       return $name;
    }



    
}
