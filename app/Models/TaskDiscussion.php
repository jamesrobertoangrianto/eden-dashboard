<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TaskDiscussion extends Model
{
    
    protected $guarded = [];  
    use HasFactory;

    function sender(): BelongsTo {
        return $this->belongsTo(Account::class, 'sender_id');
    }
}
