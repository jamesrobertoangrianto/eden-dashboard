<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Account extends Model
{
    protected $guarded = [];  
    use HasFactory;

    function role(): BelongsTo {
        return $this->belongsTo(Role::class);
    }

    function group(): BelongsTo {
        return $this->belongsTo(Group::class);
    }

    function menuAccess(): BelongsToMany {
        return $this->belongsToMany(MenuAccess::class, 'account_menu_access_relation');
    }

    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'like', '%'.$value.'%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'like', '%'.$value.'%');
    }
    
  

}
