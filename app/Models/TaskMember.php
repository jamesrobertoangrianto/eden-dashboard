<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
class TaskMember extends Model
{
    protected $guarded = [];  
    use HasFactory;

    function account(): BelongsTo {
        return $this->belongsTo(Account::class);
    }
}
