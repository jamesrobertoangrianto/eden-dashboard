<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


class NewsfeedPost extends Model
{
    protected $guarded = [];  
    protected $appends = [
        'like_count','dislike_count','pinned_count','discussion_count'
    ];

    use HasFactory;


    public function getLikeCountAttribute()
    {
        
        $total = 0;
        $performance = $this->reaction()->where('name','like')->count();
      //  $total = $performance->sum('performance_score') / 3;
        return $performance;
       
      
       
    }

    public function getDiscussionCountAttribute()
    {
        
        $total = 0;
        $performance = $this->discussion()->count();
      //  $total = $performance->sum('performance_score') / 3;
        return $performance;
       
      
       
    }


    public function getPinnedCountAttribute()
    {
        
        $total = 0;
        $performance = $this->reaction()->where('name','pinned')->count();
      //  $total = $performance->sum('performance_score') / 3;
        return $performance;
       
      
       
    }

    public function getDislikeCountAttribute()
    {
        
        $total = 0;
        $performance = $this->reaction()->where('name','dislike')->count();
      //  $total = $performance->sum('performance_score') / 3;
        return $performance;
       
      
       
    }



    function author(): BelongsTo {
        return $this->belongsTo(NewsfeedAccount::class,'author_id');
    }

    function account(): BelongsTo {
        return $this->belongsTo(Account::class,'account_id');
    }

    function reaction(): HasMany {
        return $this->hasMany(NewsfeedReaction::class);
    }



    function category(): HasMany {
        return $this->hasMany(NewsfeedPostCategory::class)->where('type','=', 'category');
    }
    function tag(): HasMany {
        return $this->hasMany(NewsfeedPostCategory::class)->where('type','=', 'tag');
    }
    function brand(): HasMany {
        return $this->hasMany(NewsfeedPostCategory::class)->where('type','=', 'brand');
    }

    function discussion(): HasMany {
        return $this->hasMany(NewsfeedDiscussion::class)->orderBy('created_at', 'desc');
    }

    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'like', '%'.$value.'%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'like', '%'.$value.'%');
    }

}
