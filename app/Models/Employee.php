<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Employee extends Model
{
    protected $guarded = [];  
    use HasFactory;

    function education(): HasMany {
        return $this->hasMany(EmployeeEducation::class);
    }
    function experience(): HasMany {
        return $this->hasMany(EmployeeExperience::class);
    }
    function performance(): HasMany {
        return $this->hasMany(EmployeePerformance::class);
    }

    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'like', '%'.$value.'%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
     
        return $query->orWhere($column, 'like', '%'.$value.'%');
    }


    function note(): HasMany {
        return $this->hasMany(Note::class);
    }

}
