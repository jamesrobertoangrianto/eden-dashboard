<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Relations\hasMany;
class CommunityReward extends Model
{ protected $guarded = [];  
    protected $appends = [
        'total_redeem'
    ];
    use HasFactory;


    function redeem(): HasMany {
        return $this->hasMany(CommunityRewardRedeem::class);
    }

    public function getTotalRedeemAttribute()
    {
        
        $total = 0;
        $total = $this->redeem()->count();
       
        return $total;
       
       
    }


}
