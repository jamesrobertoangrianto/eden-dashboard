<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

class VendorProduct extends Model
{ 
    protected $guarded = [];  
    use HasFactory;

    function vendor(): BelongsTo {
        return $this->belongsTo(Vendor::class);
    }
    function category(): BelongsTo {
        return $this->belongsTo(VendorProductCategory::class,'category_id');
    }

    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'like', '%'.$value.'%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'like', '%'.$value.'%');
    }



}
