<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CommunityPoints extends Model
{   protected $guarded = [];  
    use HasFactory;

    function member(): BelongsTo {
        return $this->belongsTo(CommunityMember::class,'community_member_id');
    }

    function points(): BelongsTo {
        return $this->belongsTo(CommunityPointsCollection::class,'activity_id');
    }


}
