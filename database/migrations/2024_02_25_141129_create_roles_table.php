<?php

use App\Models\Role;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id();
            $table->string('role_name')->nullable();
            $table->string('description')->nullable();
            $table->string('child_ids')->nullable();
            $table->boolean('can_create_account')->default(false);
            $table->boolean('is_lock')->default(false);
            $table->boolean('is_custom_role')->default(false);
            $table->boolean('add')->default(false);
            $table->boolean('edit')->default(false);
            $table->boolean('view')->default(false);
            $table->integer('position')->nullable();
            $table->timestamps();

            $table->foreignIdFor(Role::class, 'parent_id')->nullable();
        });

        DB::table('roles')->insert([
            [   
                'role_name' => 'Owner', 
                'description' => 'All access and ability to create all account type',
                'child_ids' =>  json_encode(['2','3','4','5','6']),
                'can_create_account' => true,
                'is_lock' => true,
                'is_custom_role' => false,
                'add' => true,
                'edit' => true,
                'view' => true,
            ],
            [   
                'role_name' => 'Team Leader', 
                'description' => 'All access and ability to create author, editor, viewer, guest account type',
                'child_ids' =>  json_encode(['3','4','5','6']),
                'can_create_account' => true,
                'is_lock' => false,
                'is_custom_role' => false,
                'add' => true,
                'edit' => true,
                'view' => true,
            ],
            [   
                'role_name' => 'Author', 
                'description' => 'All access without ability to create user account',
                'child_ids' =>  null,
                'can_create_account' => false,
                'is_lock' => false,
                'is_custom_role' => false,
                'add' => true,
                'edit' => true,
                'view' => true,
            ],
            [   
                'role_name' => 'Editor', 
                'description' => 'Ability to edit and view only',
                'child_ids' =>  null,
                'can_create_account' => false,
                'is_lock' => false,
                'is_custom_role' => false,
                'add' => false,
                'edit' => true,
                'view' => true,
            ],
            [   
                'role_name' => 'Viewer', 
                'description' => 'Ability view only',
                'child_ids' =>  null,
                'can_create_account' => false,
                'is_lock' => false,
                'is_custom_role' => false,
                'add' => false,
                'edit' => false,
                'view' => true,
            ],
            
          
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('roles');
    } 
};
