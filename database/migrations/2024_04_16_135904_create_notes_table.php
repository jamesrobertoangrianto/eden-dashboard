<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Creator;
use App\Models\Employee;
use App\Models\Vendor;
use App\Models\Campaign;
use App\Models\Contract;

use App\Models\Account;
use App\Models\ClientAquisition;

use App\Models\Client;
use App\Models\CommunityMember;

use App\Models\CommunityCampaign;


return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('notes', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('description')->nullable();
           

            $table->foreignIdFor(Creator::class)->nullable(); 
            $table->foreignIdFor(Employee::class)->nullable(); 
            $table->foreignIdFor(Campaign::class)->nullable(); 
            $table->foreignIdFor(CommunityCampaign::class)->nullable(); 
            $table->foreignIdFor(CommunityMember::class)->nullable(); 
            
            $table->foreignIdFor(Account::class)->nullable(); 
            $table->foreignIdFor(Client::class)->nullable(); 
            $table->foreignIdFor(ClientAquisition::class)->nullable(); 

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('notes');
    }
};
