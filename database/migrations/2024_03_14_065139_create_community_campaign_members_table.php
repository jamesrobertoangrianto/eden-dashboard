<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\CommunityMember;
use App\Models\CommunityCampaign;
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('community_campaign_members', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('status')->nullable(); 
            $table->integer('points_activity_id')->nullable(); 
            $table->integer('reward_redeem_id')->nullable(); 
            $table->foreignIdFor(CommunityMember::class)->nullable();  
            $table->foreignIdFor(CommunityCampaign::class)->nullable();  
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('community_campaign_members');
    }
};
