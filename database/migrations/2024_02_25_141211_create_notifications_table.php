<?php

use App\Models\Account;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->id();
            $table->string('notification_type')->nullable();
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->string('navigation_link')->nullable();
            $table->boolean('is_seen')->nullable()->default(false);
            $table->timestamps();

            $table->foreignIdFor(Account::class)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('notifications');
    }
};
