<?php

use App\Models\Account;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->string('labels')->nullable();
            $table->string('priority')->nullable();
            $table->string('topic_id')->nullable();
            $table->string('status')->nullable();
           
            $table->string('topic_name')->nullable();
            $table->date('due_date')->nullable();
            $table->string('attachment_url_1')->nullable();
            $table->string('attachment_url_2')->nullable();
            $table->string('attachment_url_3')->nullable();
            $table->integer('position')->nullable();
            $table->timestamps();

            $table->foreignIdFor(Account::class, 'owner_id')->nullable();   
            
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tasks');
    }
};
