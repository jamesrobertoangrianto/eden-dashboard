<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Models\CommunityMember;
use App\Models\Account;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('community_tickets', function (Blueprint $table) {
            $table->id();
            $table->timestamps();


            $table->string('topic')->nullable();
            $table->string('type')->nullable();
            $table->string('title')->nullable();
            $table->string('message')->nullable();
            $table->string('status')->nullable();
            $table->date('due_date')->nullable();
            $table->string('priority')->nullable();
            
            $table->foreignIdFor(CommunityMember::class)->nullable();
            $table->foreignIdFor(Account::class, 'create_by')->nullable();  

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('community_tickets');
    }
};
