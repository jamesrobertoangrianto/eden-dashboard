<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\NewsfeedPost;
use App\Models\NewsfeedCategory;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('newsfeed_post_categories', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('type')->nullable();
            $table->foreignIdFor(NewsfeedPost::class)->nullable(); 
            $table->foreignIdFor(NewsfeedCategory::class)->nullable(); 


        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
       
        Schema::dropIfExists('newsfeed_post_categories');
    }
};
