<?php

use App\Models\Creator;
use App\Models\Account;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('social_metrics', function (Blueprint $table) {
            $table->id();
            $table->string('platform_name')->nullable();
            $table->string('platform_id')->nullable();
            $table->string('metric_name')->nullable();
            $table->string('metric_id')->nullable();
            $table->bigInteger('value')->nullable();
         
            $table->integer('position')->nullable();
            $table->string('custom_data_1')->nullable();
            $table->string('custom_data_2')->nullable();
            $table->string('custom_data_3')->nullable();
            $table->string('custom_data_4')->nullable();
            $table->timestamps();

            $table->foreignIdFor(Creator::class, 'creator_id')->nullable();  
            $table->foreignIdFor(Account::class, 'create_by')->nullable();  
            $table->foreignIdFor(Account::class, 'update_by')->nullable();    
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('social_metrics');
    }
};
