<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Account;
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id()->startingValue(10000);
            $table->timestamps();

            $table->date('apply_date')->nullable();
            $table->date('join_date')->nullable();
            $table->date('leave_date')->nullable();
            $table->date('dob')->nullable();
            $table->string('status')->nullable();
            $table->string('photo_image_url')->nullable();


            $table->string('location')->nullable();
            $table->string('address')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('gender')->nullable();
            $table->string('religion')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('position')->nullable();


            $table->bigInteger('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('portofolio_link')->nullable();
      
            $table->bigInteger('ktp')->nullable();
            $table->string('ktp_image_url')->nullable();


            $table->bigInteger('npwp')->nullable();
            $table->string('npwp_image_url')->nullable();

            $table->string('bank_account')->nullable();
            $table->bigInteger('bpjs')->nullable();
            $table->bigInteger('salary_value')->nullable();
            $table->string('salary_image_url')->nullable();
            $table->string('cv_image_url')->nullable();
           

            $table->foreignIdFor(Account::class, 'create_by')->nullable(); 

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('employees');
    }
};
