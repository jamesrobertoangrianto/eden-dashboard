<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('newsfeed_accounts', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->unique();
            $table->boolean('status')->default(false);
            $table->string('password_hash')->nullable();
            $table->string('session_id')->nullable();
            $table->string('last_login_device')->nullable();
            $table->string('recovery_token')->nullable();
            $table->date('expired_date')->nullable();
            $table->string('profile_photo_url')->nullable();
            

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('newsfeed_accounts');
    }
};
