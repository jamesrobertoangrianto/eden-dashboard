<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\CommunityMember;

use App\Models\CreatorManagement;
use App\Models\Creator;
use App\Models\Vendor;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('client_auths', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('username')->nullable()->unique();
            $table->string('original_password')->nullable();

            $table->string('password')->nullable();
            $table->string('type')->nullable();
           
            
            $table->boolean('is_user_generate')->nullable()->default(false);
            $table->foreignIdFor(CreatorManagement::class)->nullable(); 
            
            $table->foreignIdFor(CommunityMember::class)->nullable(); 
            $table->foreignIdFor(Creator::class)->nullable(); 
            $table->foreignIdFor(Vendor::class)->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('client_auths');
    }
};
