<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;



use App\Models\Account;
use App\Models\Inventory;


return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('inventory_stocks', function (Blueprint $table) {
            $table->id()->startingValue(10000);
            $table->timestamps();


            $table->string('location')->nullable();
         
            
            $table->date('expired_date')->nullable();

            $table->string('description')->nullable();
            $table->string('sender')->nullable();
            $table->string('variant')->nullable();
            
            $table->bigInteger('stock_in')->nullable();
            $table->bigInteger('stock_out')->nullable();
            $table->string('image_url')->nullable();


            $table->foreignIdFor(Account::class, 'receive_by')->nullable(); 
            $table->foreignIdFor(Account::class, 'create_by')->nullable(); 
            $table->foreignIdFor(Inventory::class)->nullable(); 
           
         
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('inventory_stocks');
    }
};
