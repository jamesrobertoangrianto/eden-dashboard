<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\NewsfeedPost;
use App\Models\NewsfeedAccount;


return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('newsfeed_reactions', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('name')->nullable();
            $table->string('value')->nullable();
           

            $table->foreignIdFor(NewsfeedPost::class)->nullable();
            $table->foreignIdFor(NewsfeedAccount::class)->nullable();
          

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('newsfeed_reactions');
    }
};
