<?php

use App\Models\Creator;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('rate_carts', function (Blueprint $table) {
            $table->id();
            $table->string('platform_name')->nullable();
            $table->string('platform_id')->nullable();
            $table->string('rate_name')->nullable();
            $table->bigInteger('rate_price')->nullable();
            $table->string('rate_price_per')->nullable();
         
            $table->integer('position')->nullable();
            $table->string('custom_data_1')->nullable();
            $table->string('custom_data_2')->nullable();
            $table->string('custom_data_3')->nullable();
            $table->string('custom_data_4')->nullable();
            $table->timestamps();

            $table->foreignIdFor(Creator::class, 'creator_id')->nullable();  
         


        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('rate_carts');
    }
};
