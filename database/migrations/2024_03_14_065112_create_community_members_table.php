<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Account;
use App\Models\Community;
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('community_members', function (Blueprint $table) {
            $table->id()->startingValue(10000);
            $table->string('custom_member_id')->unique()->nullable();
            $table->string('status')->nullable();
            $table->timestamps();

            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('alias')->nullable();

           



            $table->string('password_hash')->nullable();


            $table->date('dob')->nullable();
            $table->string('location')->nullable();

            $table->string('province')->nullable();
            $table->string('city')->nullable();
            $table->string('district')->nullable();

            $table->string('working_status')->nullable();
            $table->string('number_of_kids')->nullable();
            $table->string('is_breast_feed')->nullable();
            $table->string('information_source')->nullable();
            $table->string('referal_code')->nullable();
            $table->string('persona')->nullable();
            $table->string('field')->nullable();
            $table->string('mothly_expense')->nullable();

            $table->string('what_to_expect')->nullable();
            $table->string('description')->nullable();






            $table->string('ktp')->nullable();
            $table->string('ktp_image_url')->nullable();

            $table->string('npwp')->nullable();
            $table->string('npwp_image_url')->nullable();
            $table->string('bank_account')->nullable();
            $table->string('bank_name')->nullable();
           
            $table->string('tier')->nullable();
           
          
      
            $table->string('profile_photo_url')->nullable();

            $table->string('instagram_account')->nullable();
            $table->string('instagram_account_link')->nullable();
            $table->string('instagram_id')->nullable();
            $table->string('instagram_follower')->nullable();
            $table->string('instagram_link')->nullable();


            $table->string('tiktok_account')->nullable();
            $table->string('tiktok_account_link')->nullable();
            $table->string('tiktok_account_id')->nullable();
            $table->string('tiktok_account_follower')->nullable();


            //community partner
            $table->string('type')->nullable();
            $table->string('community_name')->nullable();
            $table->string('member_count')->nullable();



            $table->bigInteger('phone')->nullable();
            $table->string('email')->nullable();

            $table->string('custom_data_1')->nullable();
            $table->string('custom_data_2')->nullable();
            $table->foreignIdFor(Community::class)->nullable();  

            $table->foreignIdFor(Account::class, 'create_by')->nullable();  

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('community_members');
    }
};
