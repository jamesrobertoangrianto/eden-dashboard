<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\CommunityMember;
use App\Models\Account;
use App\Models\CommunityReward;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('community_reward_redeems', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('description')->nullable();
            $table->string('type')->nullable();
            $table->string('name')->nullable();
            $table->string('value')->nullable();

            $table->string('status')->nullable();

            $table->foreignIdFor(CommunityMember::class)->nullable();
            $table->foreignIdFor(CommunityReward::class)->nullable();
            $table->foreignIdFor(Account::class, 'create_by')->nullable();  
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('community_reward_redeems');
    }
};
