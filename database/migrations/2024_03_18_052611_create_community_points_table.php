 <?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\CommunityMember;
use App\Models\Account;
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('community_points', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->bigInteger('value')->nullable();
            $table->string('description')->nullable();
            $table->string('type')->nullable();
            $table->string('referal_flag')->nullable();
            $table->integer('activity_id')->nullable();
           // $table->integer('community_campaign_id')->nullable();


            $table->foreignIdFor(CommunityMember::class)->nullable(); 
            $table->foreignIdFor(Account::class, 'create_by')->nullable();  

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('community_points');
    }
};
