<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Account;
use App\Models\Vendor;
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('vendor_products', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->integer('status')->nullable();
            $table->string('type')->nullable();
          
            $table->string('name')->nullable();
            $table->string('image_url')->nullable();
       
        
            $table->integer('category_id')->nullable();
            $table->bigInteger('stock')->nullable();
            $table->string('stock_type')->nullable();
            $table->bigInteger('price')->nullable();
            $table->string('image_url_1')->nullable();
            $table->string('image_url_2')->nullable();
            $table->string('image_url_3')->nullable();
         

            $table->foreignIdFor(Vendor::class)->nullable(); 
            $table->foreignIdFor(Account::class, 'create_by')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('vendor_products');
    }
};
