<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Account;
use App\Models\Client;
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->id();
            $table->string('campaign_name');
            $table->string('location')->nullable();
            $table->string('address')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->bigInteger('budget')->nullable();

            $table->bigInteger('creator_count')->nullable();
            $table->string('creator_tier')->nullable();

            $table->integer('status')->nullable();
           
            $table->integer('position')->nullable();
            $table->string('custom_data_1')->nullable();
            $table->string('custom_data_2')->nullable();
            $table->string('custom_data_3')->nullable();
            $table->string('custom_data_4')->nullable();
            $table->timestamps();

            $table->foreignIdFor(Client::class)->nullable();  
            $table->foreignIdFor(Account::class, 'owner_id')->nullable();  
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('campaigns');
    }
};
