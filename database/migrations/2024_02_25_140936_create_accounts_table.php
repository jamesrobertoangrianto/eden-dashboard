<?php

use App\Models\Group;

use App\Models\Account;
use App\Models\Role;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
       // Schema::dropIfExists('accounts');
        Schema::create('accounts', function (Blueprint $table) {
            $table->id();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->boolean('status')->default(false);
            $table->string('password_hash')->nullable();
            $table->string('session_id')->nullable();
            $table->string('last_login_device')->nullable();
            $table->string('recovery_token')->nullable();
            $table->date('expired_date')->nullable();
            $table->string('profile_photo_url')->nullable();
            $table->string('menu_access')->nullable();
            $table->string('custom_data_1')->nullable();
            $table->string('custom_data_2')->nullable();
            $table->string('custom_data_3')->nullable();
            $table->string('custom_data_4')->nullable();
            


            $table->timestamps();
           
            $table->foreignIdFor(Role::class)->nullable();
            $table->foreignIdFor(Group::class)->nullable();
        });

        DB::table('accounts')->insert([
            [   
                'first_name' => 'Master', 
                'status' => true, 
               // 'group_id' => 1, 
                'role_id' => 1, 

                'last_name' => 'Account', 
                'username' => 'master@edenkreasi.com', 
                'email' => 'master@edenkreasi.com', 
                'password_hash' => '1784198962', 
                'menu_access'=>'["20","21","22","5","8","1","2","10","13","11","14","6","9","15","16","17","19","28","23","24","25","27","0","3","4"]'

                
            ],
          
            
          
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('accounts');
    }
};
