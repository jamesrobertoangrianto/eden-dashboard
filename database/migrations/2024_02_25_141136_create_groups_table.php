<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->id();
            $table->string('group_name')->nullable();
            $table->string('custom_menu_access_ids')->nullable();
            $table->timestamps();
        });

        DB::table('groups')->insert([
            
            [   
                'group_name' => 'Creator', 
                'custom_menu_access_ids' => null,
            ],
            [   
                'group_name' => 'Client', 
                'custom_menu_access_ids' => null,
            ],
            [   
                'group_name' => 'Channel', 
                'custom_menu_access_ids' => null,
            ],
            [   
                'group_name' => 'Community', 
                'custom_menu_access_ids' => null,
            ],
            [   
                'group_name' => 'Creative', 
                'custom_menu_access_ids' => null,
            ],
          
            
          
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('groups');
    }
};
