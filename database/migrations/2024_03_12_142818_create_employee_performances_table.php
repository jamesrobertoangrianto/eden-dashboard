<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Models\Account;
use App\Models\Employee;



return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('employee_performances', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('performance_name')->nullable();
            $table->string('performance_score')->nullable();
   
            $table->foreignIdFor(Account::class, 'create_by')->nullable(); 
            $table->foreignIdFor(Employee::class)->nullable(); 


        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('employee_performances');
    }
};
