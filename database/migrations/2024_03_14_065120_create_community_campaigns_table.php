<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Account;
use App\Models\Community;
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('community_campaigns', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('status')->nullable();
            $table->string('campaign_name')->nullable();
            $table->bigInteger('quota')->nullable();

            $table->string('client')->nullable();
            $table->string('description')->nullable();
            $table->string('term_conditions')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();

            $table->string('reward_type')->nullable();
            $table->string('reward_value')->nullable();

            $table->string('points_type')->nullable();
            $table->string('points_value')->nullable();

            $table->foreignIdFor(Community::class)->nullable();  
            $table->foreignIdFor(Account::class, 'create_by')->nullable();  
        });
          
   
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('community_campaigns');
    }
};
