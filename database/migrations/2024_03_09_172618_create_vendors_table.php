<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Account;
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('status')->nullable();
            $table->string('type')->nullable();
            $table->string('name')->nullable();
            $table->integer('category_id')->nullable();
            $table->string('address')->nullable();
            $table->string('location')->nullable();
            $table->string('email')->nullable();
            $table->bigInteger('phone')->nullable();
            $table->string('profile_photo_url')->nullable();
            $table->string('website')->nullable();
            $table->string('social_media')->nullable();
            $table->string('portofolio')->nullable();
            $table->string('portofolio_url')->nullable();
            $table->bigInteger('rating_score')->nullable();
            $table->string('rating_name')->nullable();
            $table->bigInteger('hired')->nullable();

            $table->foreignIdFor(Account::class, 'create_by')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('vendors');
    }
};
