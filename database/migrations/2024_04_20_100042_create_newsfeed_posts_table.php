<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('newsfeed_posts', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->string('featured_image_url')->nullable();
            $table->boolean('is_featured')->default(false);
          
            $table->string('author_id')->nullable();
            $table->string('account_id')->nullable();
 
            $table->string('status')->nullable();
            

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('newsfeed_posts');
    }
};
