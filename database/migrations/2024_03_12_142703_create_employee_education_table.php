<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Models\Account;
use App\Models\Employee;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('employee_education', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('education_level')->nullable();
            $table->string('education_name')->nullable();
            $table->string('major')->nullable();
            $table->bigInteger('graduate_year')->nullable();
           
            $table->foreignIdFor(Employee::class)->nullable(); 
            $table->foreignIdFor(Account::class, 'create_by')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('employee_education');
    }
};
