<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Account;
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->string('client_name')->nullable();
            $table->string('client_type')->nullable();
            $table->string('client_category')->nullable();
            $table->string('onboard_type')->nullable();
            $table->integer('parrent_id')->nullable();

            $table->string('location')->nullable();
            $table->string('address')->nullable();
            $table->bigInteger('phone')->nullable();
            $table->string('website')->nullable();
            $table->string('social_media')->nullable();
            $table->string('profile_photo_url')->nullable();

            $table->bigInteger('total_deal_value')->nullable();
            $table->bigInteger('total_project')->nullable();
            $table->integer('on_board_status')->nullable();
            $table->integer('status')->nullable();


           
         
            $table->integer('position')->nullable();
            $table->string('custom_data_1')->nullable();
            $table->string('custom_data_2')->nullable();
            $table->string('custom_data_3')->nullable();
            $table->string('custom_data_4')->nullable();
            $table->foreignIdFor(Account::class, 'owner_id')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('clients');
    }
};
