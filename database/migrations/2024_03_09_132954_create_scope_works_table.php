<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Models\Contract;
use App\Models\ClientAquisition;
use App\Models\Account;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('scope_works', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('platform_name')->nullable();
            $table->string('platform_id')->nullable();
            $table->string('type')->nullable();
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->bigInteger('value')->nullable();



            $table->foreignIdFor(Contract::class)->nullable(); 
            $table->foreignIdFor(ClientAquisition::class)->nullable();  
            $table->foreignIdFor(Account::class, 'create_by')->nullable(); 
             
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('scope_works');
    }
};
