<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Models\Creator;
use App\Models\Contract;
use App\Models\Account;
use App\Models\Campaign;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('creator_campaigns', function (Blueprint $table) {
            $table->id();
         

           
            // $table->integer('campaign_id')->nullable();
          
            
            $table->timestamps();
            $table->foreignIdFor(Campaign::class)->nullable();  
            $table->foreignIdFor(Creator::class)->nullable();  
            $table->foreignIdFor(Contract::class)->nullable();  
            $table->foreignIdFor(Account::class, 'create_by')->nullable();  
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('creator_campaigns');
    }
};
