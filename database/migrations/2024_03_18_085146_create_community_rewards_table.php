<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Community;
use App\Models\CommunityMember;
use App\Models\Account;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('community_rewards', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->bigInteger('value')->nullable();
            $table->bigInteger('redeem_points')->nullable();
            $table->bigInteger('quota')->nullable();
            $table->string('description')->nullable();
            $table->string('type')->nullable();
            $table->string('name')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();

            
            $table->foreignIdFor(Community::class)->nullable(); 
            $table->foreignIdFor(Account::class, 'create_by')->nullable();  

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('community_rewards');
    }
};
