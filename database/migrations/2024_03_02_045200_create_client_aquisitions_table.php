<?php
use App\Models\Account;
use App\Models\Client;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('client_aquisitions', function (Blueprint $table) {
            $table->id();

            $table->string('aquisition_name')->nullable();
            $table->string('scope_of_work')->nullable();

            $table->bigInteger('budget')->nullable();
            $table->string('deal_stage')->nullable();
            $table->integer('status')->nullable();
           
         
            $table->integer('position')->nullable();
            $table->string('custom_data_1')->nullable();
            $table->string('custom_data_2')->nullable();
            $table->string('custom_data_3')->nullable();
            $table->string('custom_data_4')->nullable();
            $table->timestamps();

            $table->foreignIdFor(Client::class)->nullable();  
            $table->foreignIdFor(Account::class, 'owner_id')->nullable();  
     
           
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('client_aquisitions');
    }
};
