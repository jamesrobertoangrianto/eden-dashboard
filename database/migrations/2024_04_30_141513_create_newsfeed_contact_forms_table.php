<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('newsfeed_contact_forms', function (Blueprint $table) {
            $table->id();
            $table->timestamps();


            $table->string('first_name')->nullable();
            $table->string('email')->nullable();
            $table->BigInteger('phone')->nullable();
            $table->string('message')->nullable();


        
  

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('newsfeed_contact_forms');
    }
};
