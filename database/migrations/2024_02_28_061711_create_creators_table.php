<?php

use App\Models\Account;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('creators', function (Blueprint $table) {
          


            $table->id();
            $table->integer('status')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('alias')->nullable();

            $table->date('dob')->nullable();
            $table->string('location')->nullable();
            $table->string('persona')->nullable();

            $table->string('ktp')->nullable();
            $table->string('ktp_image_url')->nullable();

            $table->string('npwp')->nullable();
            $table->string('npwp_image_url')->nullable();
            $table->string('bank_account')->nullable();
            $table->string('bank_name')->nullable();
           
            $table->string('tier')->nullable();
           
            $table->string('company_type')->nullable();
            $table->string('company_name')->nullable();
            $table->string('partnership_type')->nullable();

            $table->string('profile_photo_url')->nullable();

         
            $table->string('custom_data_1')->nullable();
            $table->string('custom_data_2')->nullable();
            $table->string('custom_data_3')->nullable();
            $table->string('custom_data_4')->nullable();

            $table->integer('creator_management_id')->nullable();

            $table->timestamps();

            $table->foreignIdFor(Account::class, 'create_by')->nullable();  
            $table->foreignIdFor(Account::class, 'update_by')->nullable();    
           



        });


      
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('creators');
    }
};
