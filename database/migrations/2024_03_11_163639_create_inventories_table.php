<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;



use App\Models\Account;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('inventories', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('name')->nullable();
            $table->string('type')->nullable();
           // $table->string('category')->nullable();
            $table->integer('category_id')->nullable();

            $table->string('status')->nullable();

            $table->string('location')->nullable();
            $table->string('image_url')->nullable();
            

            $table->string('cost')->nullable();
            $table->date('expired_date')->nullable();
            $table->string('description')->nullable();
            $table->string('brand')->nullable();
         
            $table->date('last_stock_in')->nullable();
            $table->date('last_stock_out')->nullable();

            $table->foreignIdFor(Account::class, 'create_by')->nullable(); 
            $table->foreignIdFor(Account::class, 'pic')->nullable(); 


        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('inventories');
    }
};
