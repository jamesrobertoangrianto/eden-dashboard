<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;



use App\Models\CommunityTicket;

use App\Models\CommunityMember;
use App\Models\Account;


return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('community_ticket_discussions', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
           
            $table->string('message')->nullable();
            $table->string('type')->nullable();
        
            $table->boolean('is_read')->default(false);

            $table->foreignIdFor(CommunityTicket::class)->nullable();
            $table->foreignIdFor(CommunityMember::class)->nullable();
            $table->foreignIdFor(Account::class)->nullable();  
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('community_ticket_discussions');
    }
};
