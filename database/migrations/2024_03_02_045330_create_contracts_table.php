<?php

use App\Models\Campaign;
use App\Models\Creator;
use App\Models\Client;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->id()->startingValue(10000);
            $table->string('custom_contract_id')->unique()->nullable();
            $table->string('status')->nullable();
       
            $table->string('invoice_number')->nullable();
            
            $table->string('legal_doc_url')->nullable();
            $table->string('term_of_payment')->nullable();
            $table->string('term_of_payment_1')->nullable();


            $table->bigInteger('budget')->nullable();
            $table->bigInteger('gross_up')->nullable();

            $table->integer('position')->nullable();
            $table->string('custom_data_1')->nullable();
            $table->string('custom_data_2')->nullable();
            $table->string('custom_data_3')->nullable();
            $table->string('custom_data_4')->nullable();
            $table->timestamps();
            $table->foreignIdFor(Campaign::class)->nullable();  
            $table->foreignIdFor(Creator::class)->nullable();  
            $table->foreignIdFor(Client::class)->nullable();  
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('contracts');
    }
};
